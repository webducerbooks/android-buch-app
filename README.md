# Android-Apps programmieren

**Der Quellcode zu dieser Auflage wird nicht mehr aktualisiert**

In diesem Projekt finden Sie den Quellcode zu meinem Buch "Android-Apps programmieren" bei [**mitp**-Verlag](https://wdurl.de/ab-mitp).

- [Quelcode Herunterladen](https://bitbucket.org/webducerbooks/android-buch-app/get/master.zip)

## Korrekturen

### 06.03.2020

- Aktualisierung der Abhängigkeiten für Android Studio 3.6.1
	- Gradle Plugin von Version `3.5.1` auf `3.6.1`
	- Gradle von Version `5.4.1` auf `5.6.4`

### 20.10.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.5.1
	- OkHttp Bibliothek von Version `3.14.1` auf `4.2.2`
	- Gradle Plugin von Version `3.4.0` auf `3.5.1`
	- Gson Bibliothek von Version `2.8.5` auf `2.8.6`
	- Gradle von Version `5.1.1` auf `5.4.1`

### 22.04.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.4.0 Kompatibilität
	- Google Gradle Plugin von Version `3.3.1` auf `3.4.0`
	- OkHttp Bibliothek von Version `3.11.0` auf `3.14.1`
	- Gradle von Version `4.1.0.1` auf `5.1.1`
	- Android test runner von Version `0.5` auf `1.0.2`
	- Android Espresso test runner von Version `2.2.2` auf `3.0.2`

### 08.02.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.3.1 Kompatibilität
	- Google Gradle Plugion von Version `3.3.0` auf `3.3.1`

### 15.01.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.3 Kompatibilität
	- Google gradle Plugin von Version `3.2.1` auf `3.3.0`
	- Gradle von Version `4.6` auf `4.10.1`

### 18.11.2018

- Quellcode an Android Studio 3.2 angeglichen:
	- Gradle Wrapper von Version 4.4 zu **4.6**
	- Graddle Plugin von Version 3.1.0 zu **3.2.1**
	- Support-Bibliotheken von Version 27.1.1 zu **28.0.0**
	- Contraint-Layout Bibliothek von Version 1.0.2 zu **1.1.3**
	- Mockito-Bibliothek von Version 2.10.0 zu **2.23.0**
	- GSon-Bibliothek von Version 2.8.2 zu **2.8.5**
	- OK-HTTP-Bibliothek von Version 3.9.0 zu **3.11.0**

### 08.04.2018

- Quellcode an Android Studio 3.1 angeglichen
	- Aktualisierung von Gradle von Version 4.1 auf **4.4** (Datei `gradle-wrapper.properties`)
	- Aktualisierung des Android-Gradle Plugins von Version 3.0.1 auf **3.1.0**
	- Aktualisierung der Support-Bibliotheken von Version 27.0.2 auf **27.1.1**

### 17.02.108

- Quellcode an Android Studio 3.0.1 angeglichen
	- Änderung SDK Version von 26 auf **27**
	- Änderung der Build-Tool Version von 26.0.2 auf **27.0.3**
	- Änderung der Support-Bibliothek Version von 26.1.0 auf **27.0.2**
	- Hinzufügen der _neuen_ `rounded` Icons für App-Store

### 03.11.2017

- [DB Browser for SQLite statt SQLite Manager](https://wdurl.de/ab-kor-01)
    - S. 116 - 118
    - S. 118 - 120
    - S. 124
    - S. 131
    - S. 241 - 246
- [Android Device Monitor ist Geschichte](https://wdurl.de/ab-kor-02)
    - S. 130
    - S. 212 - 213
    - S. 218
- Quellcode an Android Studio 3.0 angeglichen
    - Änderung der Gradle Version von 3.3 auf **4.1**
    - Hinzufügen von `google` als ein zusätzliches Maven-Repository (Support-Bibliotheken)
    - Änderung des Android-Gradle-Plugin Version von 2.3.3 auf **3.0.1**
    - Ändern der SDK Version von 25 auf **26**
    - Ändern der Build-Tool Version von 26.0.1 auf **26.0.2**
    - Ändern der Support-Bibliothek Version von 25.3.1 auf **26.1.0**