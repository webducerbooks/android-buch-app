package de.webducer.androidbuch.zeiterfassung;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  private DateFormat _dateTimeFormatter = DateFormat.getDateTimeInstance(
      DateFormat.SHORT, DateFormat.SHORT);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Eingabefeld für Startzeit suchen
    _startDateTime = (EditText) findViewById(R.id.StartDateTime);
    _endDateTime = (EditText) findViewById(R.id.EndDateTime);
    _startCommand = (Button) findViewById(R.id.StartCommand);
    _endCommand = (Button) findViewById(R.id.EndCommand);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Button Klick registrieren
    _startCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Calendar currentTime = Calendar.getInstance();
        _startDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));
      }
    });
    _endCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Calendar currentTime = Calendar.getInstance();
        _endDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));
      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Buttin Klick deregisitrieren
    _startDateTime.setOnClickListener(null);
    _endDateTime.setOnClickListener(null);
  }
}
