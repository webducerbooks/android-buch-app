package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Eingabefeld für Startzeit suchen
    _startDateTime = (EditText) findViewById(R.id.StartDateTime);
    _endDateTime = (EditText) findViewById(R.id.EndDateTime);
    _startCommand = (Button) findViewById(R.id.StartCommand);
    _endCommand = (Button) findViewById(R.id.EndCommand);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Button Klick registrieren
    _startCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Logging
        Log.d(
            "MainActivity", // TAG
            "onClick für Start-Button aufgerufen"); // Nachricht
        // Toast in der UI
        Toast.makeText(
            MainActivity.this, // Context
            "onClick für Start-Button aufgerufen", // Nachricht
            Toast.LENGTH_SHORT) // Anzeigedauer
            .show();

        Calendar currentTime = Calendar.getInstance();
        _startDateTime.setText(currentTime.getTime().toString());
      }
    });
    _endCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Calendar currentTime = Calendar.getInstance();
        _endDateTime.setText(currentTime.getTime().toString());
      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Buttin Klick deregisitrieren
    _startDateTime.setOnClickListener(null);
    _endDateTime.setOnClickListener(null);
  }
}
