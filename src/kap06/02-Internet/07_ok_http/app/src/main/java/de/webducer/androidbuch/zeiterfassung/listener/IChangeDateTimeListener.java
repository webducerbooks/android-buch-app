package de.webducer.androidbuch.zeiterfassung.listener;

import android.widget.TextView;

import java.util.Calendar;

public interface IChangeDateTimeListener {
  String START_DATE = "StartDate";
  String END_DATE = "EndDate";
  String START_TIME = "StartTime";
  String END_TIME = "EndTime";

  /**
   * Instanz für die Bearbeitung
   *
   * @param dialogType Typ des Dialoges (siehe Konstanten in diesem Interface)
   * @return
   */
  Calendar getDate(String dialogType);

  /**
   * View, in dem die neuen Daten aktualisiert werden sollen
   *
   * @param dialogType Typ des Dialoges (siehe Konstanten in diesem Interface)
   * @return
   */
  TextView getOutputView(String dialogType);
}
