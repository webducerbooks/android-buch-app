package de.webducer.androidbuch.zeiterfassung;

import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ListView;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.utils.IssueAdapter;
import de.webducer.androidbuch.zeiterfassung.utils.IssueCursorAdapter;
import de.webducer.androidbuch.zeiterfassung.utils.IssueUpdater;

public class InfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

  private final static int _ISSUE_LOADER = 100;
  private ListView _list;
  private SimpleCursorAdapter _adapter;

  // Spalten zum Laden
  private static final String[] _COLUMNS = new String[]{
      BaseColumns._ID,
      TimeDataContract.IssueData.Columns.NUMBER,
      TimeDataContract.IssueData.Columns.TITLE,
      TimeDataContract.IssueData.Columns.PRIORITY,
      TimeDataContract.IssueData.Columns.STATE
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Intialisierung der UI Elemente
    _list = (ListView) findViewById(R.id.IssueList);

    // Zuweisung des Adapters
    _adapter = new IssueCursorAdapter(this, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    _list.setAdapter(_adapter);

    // Laden der Fehlerliste
    new IssueUpdater(getApplicationContext()).execute();
  }

  @Override
  protected void onStart() {
    super.onStart();

    getSupportLoaderManager().restartLoader(_ISSUE_LOADER, null, this);
  }

  @Override
  protected void onStop() {
    super.onStop();

    getSupportLoaderManager().destroyLoader(_ISSUE_LOADER);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    CursorLoader loader = null;

    switch (id){
      case _ISSUE_LOADER:
        loader = new CursorLoader(
            this,
            TimeDataContract.IssueData.CONTENT_URI,
            _COLUMNS, // Spalten
            null,
            null,
            TimeDataContract.IssueData.Columns.NUMBER + " DESC"); // Sortierung
    }

    return loader;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    switch (loader.getId()){
      case _ISSUE_LOADER:
        _adapter.swapCursor(data);
        break;
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    switch (loader.getId()) {
      case _ISSUE_LOADER:
        _adapter.swapCursor(null);
        break;
    }
  }
}
