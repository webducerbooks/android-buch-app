package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class IssueUpdater extends AsyncTask<Void, Void, Void> {
  // Basis-Zugriffspunkt
  private final static String _ISSUE_BASE_URI =
      "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues?q=";

  // Filter
  private final static String _ISSUE_FILTER =
      "(state = \"new\" OR state = \"on hold\" OR state = \"open\")"
          + " AND updated_on > \"2017-02-01T00:00+02:00\""
          + " AND component != \"395592\"";
  private final static String _FILTERED_ISSUE_URI =
      _ISSUE_BASE_URI
          + Uri.encode(_ISSUE_FILTER);

  // Standardrückgabewert
  private final static BitbucketIssue[] _DEFAULT_RETURN_VALUE = new BitbucketIssue[]{};
  private final Context _context;

  public IssueUpdater(Context context) {
    _context = context;
  }

  @Override
  protected Void doInBackground(Void... params) {
    try {
      // Client initialisieren
      OkHttpClient client = new OkHttpClient();

      // Anfrage erstellen
      Request request = new Request.Builder()
          .url(_FILTERED_ISSUE_URI)
          .build();

      Response response = client.newCall(request).execute();

      if (!response.isSuccessful()) {
        // Fehler bei der Abfrage der Daten
        return null;
      }

      // Zurückgeben der Daten
      // Parsen mit GSon
      BitbucketIssue[] issues = parseWithGSon(response.body().string());

      // Speichern in die Datenbank
      saveToDb(issues);
      return null;
    } catch (IOException e) {
      // Fehler beim Zugriff auf das Internet
      e.printStackTrace();
    }

    return null;
  }

  // Parsen der Antwort mit Bibliothek GSon von Google
  private BitbucketIssue[] parseWithGSon(String content) {
    // Prüfen des Inhaltes
    if (content == null || content.isEmpty()) {
      // Rückgabe einer leeren Liste
      return _DEFAULT_RETURN_VALUE;
    }

    // Initialisieren der Bibliothek
    Gson g = new Gson();
    // Deserialisieren von JSON
    BitbucketResponse response = g.fromJson(content, BitbucketResponse.class);

    for (BitbucketIssue issue : response.values) {
      Log.i("ISSUE-TITLE: ", issue.title);
    }

    return response.values;
  }

  private void saveToDb(BitbucketIssue[] issues) {
    Uri updateUri = null;
    ContentValues values = new ContentValues();
    for (BitbucketIssue issue : issues) {
      // Uri definieren
      updateUri = ContentUris.withAppendedId(TimeDataContract.IssueData.BY_NUMBER_CONTENT_URI, issue.id);
      // Spaltenwerte füllen
      values.put(TimeDataContract.IssueData.Columns.NUMBER, issue.id);
      values.put(TimeDataContract.IssueData.Columns.TITLE, issue.title);
      values.put(TimeDataContract.IssueData.Columns.PRIORITY, issue.priority);
      values.put(TimeDataContract.IssueData.Columns.STATE, issue.state);

      // Speichern
      _context.getContentResolver().update(updateUri, values, null, null);
    }
  }
}
