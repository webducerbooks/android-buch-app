package de.webducer.androidbuch.zeiterfassung;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.dialogs.DeleteTimeDataDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.ExportDialog;
import de.webducer.androidbuch.zeiterfassung.utils.CsvExporter;
import de.webducer.androidbuch.zeiterfassung.utils.DateTimeViewBinder;
import de.webducer.androidbuch.zeiterfassung.utils.ExportService;

public class ListDataActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
  // Klassenvariablen
  private ListView _list;
  private SimpleCursorAdapter _adapter; // Adapter für die Listendarstellung
  private final static int _DATA_LOADER_ID = 100; // ID des Loaders (Hintergrundladen)
  private final static String[] _LOAD_COLUMNS = new String[]{
      TimeDataContract.TimeData.Columns._ID,
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME
  }; // Spalten zum Laden
  private final static String[] _VISIBLE_COLUMNS = new String[]{
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME
  }; // Sichtbare Spalten
  private final static int[] _ROW_VIEW_IDS = new int[]{
      R.id.StartTimeValue,
      R.id.EndTimeValue
  }; // IDs für Zeilen-Views
  private final static String _SORT_ORDER =
      TimeDataContract.TimeData.Columns.START_TIME + " DESC";
  private final static int _REQUEST_WRITE_PERMISSION_ID = 100; // ID für die Berechtigungsabfrage
  private final static int _SAF_CREATE_EXPORT_FILE = 200; // ID für SAF Dateiabfrage


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_data);

    // Suchen der Liste im Layout
    _list = (ListView) findViewById(R.id.DataList);

    // Initialisierung des Adapters
    _adapter = new SimpleCursorAdapter(
        this, // Context
        R.layout.row_time_data, // Layout für die Zeile
        null, // Daten für die Darstellung
        _VISIBLE_COLUMNS, // Darzustellende Spalten
        _ROW_VIEW_IDS, // IDs der Views für die Darstellung
        android.support.v4.widget.SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
    ); // Änderungen an den Daten beobachten

    // Formatierung der Daten im Adapter beeinflussen
    _adapter.setViewBinder(new DateTimeViewBinder());

    // Adapter der Liste zuordnen
    _list.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Loader starten
    getSupportLoaderManager().restartLoader(
        _DATA_LOADER_ID, // ID des Loaders, der gestartet werden soll
        null, // keine Zusatzinformationen an Loader übergeben
        this); // Klasse, die das Loader-Interface implementiert
  }

  @Override
  protected void onStop() {
    super.onStop();

    // Loader freigeben
    getSupportLoaderManager().destroyLoader(_DATA_LOADER_ID);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Registrieren des Kontextmenüs für die Liste
    registerForContextMenu(_list);

    // Listener registrieren
    _list.setOnItemClickListener(new OnItemClicked());
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Deregistrieren des Kontextmenüs für die Liste
    unregisterForContextMenu(_list);

    // Listener deregistrieren
    _list.setOnItemClickListener(null);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_data_list, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.ExportMenuItem:
        selectFileForExport();
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    switch (v.getId()) {
      case R.id.DataList:
        // Kontextmenü für die Liste aufbauen
        getMenuInflater().inflate(R.menu.ctx_menu_data_list, menu);
        break;
    }
    super.onCreateContextMenu(menu, v, menuInfo);
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    // Metainformationen zum Menüpunkt auslesen
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

    switch (item.getItemId()) {
      case R.id.DeleteMenuItem:
        deleteItem(info.id);
        return true;

      case R.id.EditMenuItem:
        editItem(info.id);
        return true;

      default:
        return super.onContextItemSelected(item);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK && requestCode == _SAF_CREATE_EXPORT_FILE) {
      // Pfad zur Deitei (als Content Provider URI)
      Uri fileUri = data.getData();
      exportData(fileUri);
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
    CursorLoader loader = null;

    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loaderId) {
      case _DATA_LOADER_ID:
        loader = new CursorLoader(
            this, // Context
            TimeDataContract.TimeData.CONTENT_URI, // URI für den Content Provider
            _LOAD_COLUMNS, // Zu ladende Spalten
            null, // Filter
            null, // Filter Argumente
            _SORT_ORDER // Sortierung
        );
        break;
    }

    return loader;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(data); // Austauschen der Daten gegen neue im Adapter
        break;
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(null); // Daten freigeben
        break;
    }
  }

  private void editItem(long id) {
    // Erstellen neues Intents für Bearbeitung
    Intent editIntent = new Intent(this, EditActivity.class);
    // ID als Metainformation an Intent binden
    editIntent.putExtra(EditActivity.ID_KEY, id);
    // Bearbeitungs-Activity starten
    startActivity(editIntent);
  }

  private void deleteItem(long id) {
    // Argumente initialisieren
    Bundle arguments = new Bundle();
    arguments.putLong(DeleteTimeDataDialog.ID_KEY, id);

    // Dialog initialisieren
    DeleteTimeDataDialog dialog = new DeleteTimeDataDialog();
    dialog.setArguments(arguments);

    // Dialog anzeigen
    dialog.show(getSupportFragmentManager(), "DeleteDialog");
  }

  private void selectFileForExport() {
    // Aktion, eine neue Datei anzulegen
    Intent fileIntent = new Intent(Intent.ACTION_CREATE_DOCUMENT);

    // Kategorie, um diese Datei dann auch öffnen zu können
    fileIntent.addCategory(Intent.CATEGORY_OPENABLE);

    // Datentyp (Mime Type) setzen, als Filter
    fileIntent.setType("text/csv");

    // Vorschlag für den Namen der Datei
    fileIntent.putExtra(Intent.EXTRA_TITLE, "export.csv");

    // Starten des Auswahlfensters
    startActivityForResult(fileIntent, _SAF_CREATE_EXPORT_FILE);
  }

  private void exportData(final Uri exportFile) {
    // Dialog anzeigen (erst, wenn die Oberfläche dazu fertig ist)
    _list.post(new Runnable() {
      @Override
      public void run() {
        // Dialog starten
        ExportDialog dialog = new ExportDialog();
        dialog.show(getSupportFragmentManager(), "ExportDialog");

        // Service für Export initialisieren
        Intent exportService = new Intent(
            ListDataActivity.this, // Context
            ExportService.class); // Service, dass gestartet werden soll
        // Dateinamen für den Export mitsenden
        exportService.setData(exportFile);
        exportService.setAction(ExportService.ACTION_START_EXPORT); // Action auf Export setzen

        // Service starten
        startService(exportService);
      }
    });
  }

  class OnItemClicked implements AdapterView.OnItemClickListener {

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      // Ausgewähltes Element bearbeiten
      editItem(id);
    }
  }
}
