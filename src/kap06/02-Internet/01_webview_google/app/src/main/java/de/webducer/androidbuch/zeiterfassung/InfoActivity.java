package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class InfoActivity extends AppCompatActivity {

  private WebView _webView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Intialisierung der UI Elemente
    _webView = (WebView) findViewById(R.id.WebContent);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Google-Seite
    _webView.loadUrl("https://www.google.de");
  }
}
