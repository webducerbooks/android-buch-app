package de.webducer.androidbuch.zeiterfassung.utils;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketResponse;

public class IssueUpdater extends AsyncTask<Void, Void, BitbucketIssue[]> {
  // Basis-Zugriffspunkt
  private final static String _ISSUE_BASE_URI =
      "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues?q=";

  // Filter
  private final static String _ISSUE_FILTER =
      "(state = \"new\" OR state = \"on hold\" OR state = \"open\")"
          + " AND updated_on > \"2017-02-01T00:00+02:00\""
          + " AND component != \"395592\"";
  private final static String _FILTERED_ISSUE_URI =
      _ISSUE_BASE_URI
          + Uri.encode(_ISSUE_FILTER);

  // Standardrückgabewert
  private final static BitbucketIssue[] _DEFAULT_RETURN_VALUE = new BitbucketIssue[]{};
  private final IssueAdapter _adapter;

  public IssueUpdater(IssueAdapter adapter) {

    _adapter = adapter;
  }

  // Methode, um nach dem Abschluss der Arbeit Daten zu verarbeiten
  @Override
  protected void onPostExecute(BitbucketIssue[] issues) {
    super.onPostExecute(issues);

    // Adapter leeren
    _adapter.clear();
    // Adapter mit neuen Werten befüllen
    _adapter.addAll(issues);
  }

  @Override
  protected BitbucketIssue[] doInBackground(Void... params) {
    // Client initialisieren
    try {
      // Parsen der Uri
      URL url = new URL(_FILTERED_ISSUE_URI);

      // Erstellen des Clients
      HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

      // Defintion der Timeouts
      connection.setReadTimeout(60000); // 60 Sekunden
      connection.setConnectTimeout(60000); // 60 Sekunden

      // Anfrage Header definieren
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Accept", "application/json");

      // Verbinden
      connection.connect();

      // Status der Anfrage prüfen
      int statusCode = connection.getResponseCode();
      if (statusCode != 200) {
        // Fehler bei der Abfrage der Daten
        return _DEFAULT_RETURN_VALUE;
      }

      // Laden der Daten
      InputStream is = connection.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));

      // Lesen der geladenen Daten
      StringBuilder content = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        content.append(line);
      }

      // Schließen der Resourcen
      reader.close();
      is.close();
      connection.disconnect();

      // Zurückgeben der Daten
      // Parsen mit GSon
      return parseWithGSon(content.toString());

    } catch (MalformedURLException e) {
      // URL konnte nicht interpretiert werden
      e.printStackTrace();
    } catch (IOException e) {
      // Fehler beim Zugriff auf das Internet
      e.printStackTrace();
    }

    return _DEFAULT_RETURN_VALUE;
  }

  // Parsen der Antwort mit Bibliothek GSon von Google
  private BitbucketIssue[] parseWithGSon(String content) {
    // Prüfen des Inhaltes
    if (content == null || content.isEmpty()) {
      // Rückgabe einer leeren Liste
      return _DEFAULT_RETURN_VALUE;
    }

    // Initialisieren der Bibliothek
    Gson g = new Gson();
    // Deserialisieren von JSON
    BitbucketResponse response = g.fromJson(content, BitbucketResponse.class);

    for (BitbucketIssue issue : response.values) {
      Log.i("ISSUE-TITLE: ", issue.title);
    }

    return response.values;
  }
}
