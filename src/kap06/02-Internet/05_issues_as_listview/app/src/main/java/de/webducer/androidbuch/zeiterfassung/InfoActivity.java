package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ListView;

import de.webducer.androidbuch.zeiterfassung.utils.IssueAdapter;
import de.webducer.androidbuch.zeiterfassung.utils.IssueUpdater;

public class InfoActivity extends AppCompatActivity {

  private ListView _list;
  private IssueAdapter _adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Intialisierung der UI Elemente
    _list = (ListView) findViewById(R.id.IssueList);

    // Zuweisung des Adapters
    _adapter = new IssueAdapter(this);
    _list.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Fehlerliste
    new IssueUpdater(_adapter).execute();
  }
}
