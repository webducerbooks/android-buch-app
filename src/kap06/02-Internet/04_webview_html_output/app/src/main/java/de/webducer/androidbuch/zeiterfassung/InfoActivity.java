package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import de.webducer.androidbuch.zeiterfassung.utils.IssueUpdater;

public class InfoActivity extends AppCompatActivity {

  private WebView _webView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Intialisierung der UI Elemente
    _webView = (WebView) findViewById(R.id.WebContent);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Google-Seite
    new IssueUpdater(_webView).execute();
  }
}
