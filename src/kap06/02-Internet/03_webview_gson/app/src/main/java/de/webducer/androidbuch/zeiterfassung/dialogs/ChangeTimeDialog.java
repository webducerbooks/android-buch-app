package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.listener.IChangeDateTimeListener;

public class ChangeTimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
  // Klassenvariablen
  private DateFormat _timeFormatter;

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IChangeDateTimeListener == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IChangeDateTimeListener in your calling activity");
    }

    // Formatter initialisieren
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(getContext());

    // Interface initialisieren
    IChangeDateTimeListener listener = (IChangeDateTimeListener) getActivity();
    Calendar date = listener.getDate(getTag());

    // Einstellung für 24 Stunden Anzeige im Betriebssystem
    boolean is24Format = android.text.format.DateFormat.is24HourFormat(getContext());

    // Initialisieren des Dialoges
    TimePickerDialog dialog = new TimePickerDialog(getContext(),
        this, // Änderungscallback
        date.get(Calendar.HOUR_OF_DAY), // Stunden
        date.get(Calendar.MINUTE), // Minuten
        is24Format); // 24 Stunden Anzeige?

    return dialog;
  }

  @Override
  public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IChangeDateTimeListener == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IChangeDateTimeListener in your calling activity");
    }

    // Interface initialisieren
    IChangeDateTimeListener listener = (IChangeDateTimeListener) getActivity();

    // Datum ändern
    Calendar date = listener.getDate(getTag());
    date.set(Calendar.HOUR_OF_DAY, hourOfDay);
    date.set(Calendar.MINUTE, minute);

    // Ausgabe anpassen
    TextView outputView = listener.getOutputView(getTag());
    if (outputView != null) {
      outputView.setText(_timeFormatter.format(date.getTime()));
    }
  }
}
