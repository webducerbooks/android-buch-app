package de.webducer.androidbuch.zeiterfassung.bindingadapter;


import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.widget.TextView;

public class IntegerBindingAdapter {
  @BindingAdapter("android:text")
  public static void setText(TextView view, int value) {
    // "0" als "" darstellen
    String newValue = value == 0 ? "" : Integer.toString(value);
    if(newValue.equals(view.getText().toString())){
      // Kein neuer Wert
      return;
    }
    view.setText(newValue);
  }

  @InverseBindingAdapter(attribute = "android:text")
  public static int getText(TextView view){
    if (view.getText() == null || "".equals(view.getText().toString())){
      // Wenn kein Text gesetzt ist, Standard "0" zurückgeben
      return 0;
    }

    String stringValue = view.getText().toString();
    int returnValue = 0;

    // Konvertierung
    try {
      returnValue = Integer.parseInt(stringValue);
    } catch (NumberFormatException e){
      // Eingegebener Wert ist keine Zahl, Standard "0" zurückgeben.
      e.printStackTrace();
    }

    return returnValue;
  }
}
