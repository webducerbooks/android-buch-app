package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityModernEditBinding;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateModernDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeTimeModernDialog;
import de.webducer.androidbuch.zeiterfassung.listener.IDualogService;
import de.webducer.androidbuch.zeiterfassung.viewmodels.EditViewModel;

public class ModernEditActivity extends AppCompatActivity implements IDualogService {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";
  // Konstanten für die Zustandssicherung
  private static final String _START_DATE_TIME_KEY = "StartTimeKey";
  private static final String _END_DATE_TIME_KEY = "EndTimeKey";
  private static final String _PAUSE_KEY = "PauseKey";
  private static final String _COMMENT_KEY = "CommentKey";

  // Klassenvariablen
  private static final long _NO_DATA = -1L;
  private long _timeTadaId = _NO_DATA;
  private boolean _isRestored = false;

  // View Models
  private EditViewModel _viewModel = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Bindung initialisieren
    ActivityModernEditBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_modern_edit);
    // View Model initialisieren
    _viewModel = new EditViewModel();
    // View Model mit Layout verbinden
    binding.setEditData(_viewModel);

    // Manuelle Eingaben verhindern
    binding.StartDate.setKeyListener(null);
    binding.StartTime.setKeyListener(null);
    binding.EndDate.setKeyListener(null);
    binding.EndTime.setKeyListener(null);

    // Auslesen der übergebenen Metainformationen, falls welche da sind
    _timeTadaId = getIntent().getLongExtra(
      ID_KEY, // Key für die metainformation
      _NO_DATA); // Standardwert, falls nichts übergeben wurde

    // Wiederherstellen-Flag der Daten
    if (savedInstanceState != null && savedInstanceState.containsKey(_START_DATE_TIME_KEY)) {
      // Flag für Wiederherstellung
      _isRestored = true;
    }
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Dialog Service setzen
    _viewModel.setDialogService(this);

    // Daten laden, wenn Daten nicht wiederhergestellt wurden
    if (!_isRestored) {
      loadData();
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

    // Dialogservice wieder entziehen
    _viewModel.setDialogService(null);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    // Speichern der Daten, wenn der Benutzer zurück geht
    saveData();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Zurück-Button in Actionbar
      case android.R.id.home:
        saveData();

        // Kein break oder return, da dieses Menü von Android noch verarbeitet werden soll

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    // Datumsfelder als Anzahl der Millisekunden merken
    outState.putLong(_START_DATE_TIME_KEY, _viewModel.getStartTime().getTimeInMillis());
    outState.putLong(_END_DATE_TIME_KEY, _viewModel.getEndTime().getTimeInMillis());

    // Pause nach der Konvertierung in Integer merken
    outState.putInt(_PAUSE_KEY, _viewModel.getPause());

    // Kommentar sichern
    outState.putString(_COMMENT_KEY, _viewModel.getComment());
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    // Wiederherstellen der Daten
    if (savedInstanceState != null && savedInstanceState.containsKey(_START_DATE_TIME_KEY)) {
      // Datumsfelder aus der Anzahl der Millisekunden
      Calendar date = Calendar.getInstance();
      date.setTimeInMillis(savedInstanceState.getLong(_START_DATE_TIME_KEY));
      _viewModel.setStartTime(date);

      date = Calendar.getInstance();
      date.setTimeInMillis(savedInstanceState.getLong(_END_DATE_TIME_KEY));
      _viewModel.setEndTime(date);

      // Pause, falls gesichert (mit Standardwert)
      _viewModel.setPause(savedInstanceState.getInt(_PAUSE_KEY, 0));

      // Kommentar
      _viewModel.setComment(savedInstanceState.getString(_COMMENT_KEY));
    }
  }

  @Override
  public void changeDate(@NonNull Calendar originDate, int fieldId) {
    // Dialog starten
    ChangeDateModernDialog.showDialog(getSupportFragmentManager(), originDate, fieldId);
  }

  @Override
  public void changeTime(@NonNull Calendar originTime, int fieldId) {
    // Dialog starten
    ChangeTimeModernDialog.showDialog(getSupportFragmentManager(), originTime, fieldId);
  }

  @Override
  public void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldId) {
    // View Model benachrichtigen
    _viewModel.notifyDateChenaged(newDateTime, fieldId);
  }

  private void loadData() {
    Uri dataUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeTadaId);
    Cursor data = getContentResolver().query(dataUri, // URI
      null, // Spalten
      null, // Filter
      null, // Filter Argumente
      null); // Sortierung

    // Prüfen, ob Daten zu der ID vorhanden sind
    if (data.moveToFirst()) {
      // Auslesen der Startzeit
      String startDateString = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME));
      try {
        _viewModel.setStartTime(TimeDataContract.Converter.parse(startDateString));
      } catch (ParseException e) {
        // Startzeit konnte nicht interpretiert werden
        e.printStackTrace();
      }

      // Auslesen der Endzeit
      int columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
      if (!data.isNull(columnIndex)) {
        String endDateString = data.getString(columnIndex);
        try {
          _viewModel.setEndTime(TimeDataContract.Converter.parse(endDateString));
        } catch (ParseException e) {
          // Endzeit konnte nicht interpretiert werden
          e.printStackTrace();
        }
      }

      // Auslesen und Ausgabe der Pause
      _viewModel.setPause(data.getInt(data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE)));

      // Auslesen und Ausgabe des Kommentars
      columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.COMMENT);
      if (!data.isNull(columnIndex)) {
        _viewModel.setComment(data.getString(columnIndex));
      }
    }

    // Ressourcen freigeben
    data.close();
  }

  private void saveData() {
    // Werte den Spalten zuordnen
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.Converter.format(_viewModel.getStartTime()));
    values.put(TimeDataContract.TimeData.Columns.END_TIME,
      TimeDataContract.Converter.format(_viewModel.getEndTime()));
    values.put(TimeDataContract.TimeData.Columns.PAUSE, _viewModel.getPause());
    values.put(TimeDataContract.TimeData.Columns.COMMENT, _viewModel.getComment());

    // Speichern der Werte in der Datenbank
    Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeTadaId);
    getContentResolver().update(updateUri, values, null, null);
  }
}
