package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketResponse;

public final class IssueHandler {
  // Basis-Zugriffspunkt
  private final static String _ISSUE_BASE_URI =
      "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues?q=";

  // Filter
  private final static String _ISSUE_FILTER =
      "(state = \"new\" OR state = \"on hold\" OR state = \"open\")"
          + " AND updated_on > \"2017-02-01T00:00+02:00\""
          + " AND component != \"395592\"";
  public final static String FILTERED_ISSUE_URI =
      _ISSUE_BASE_URI
          + Uri.encode(_ISSUE_FILTER);

  private final static BitbucketIssue[] _DEFAULT_RETURN_VALUE = new BitbucketIssue[]{};

  // Parsen der Antwort mit Bibliothek GSon von Google
  public static BitbucketIssue[] parseWithGSon(String content) {
    // Prüfen des Inhaltes
    if (content == null || content.isEmpty()) {
      // Rückgabe einer leeren Liste
      return _DEFAULT_RETURN_VALUE;
    }

    // Initialisieren der Bibliothek
    Gson g = new Gson();
    // Deserialisieren von JSON
    BitbucketResponse response = g.fromJson(content, BitbucketResponse.class);

    for (BitbucketIssue issue : response.values) {
      Log.i("ISSUE-TITLE: ", issue.title);
    }

    return response.values;
  }

  public static void saveToDb(Context context, BitbucketIssue[] issues) {
    Uri updateUri = null;
    ContentValues values = new ContentValues();
    for (BitbucketIssue issue : issues) {
      // Uri definieren
      updateUri = ContentUris.withAppendedId(TimeDataContract.IssueData.BY_NUMBER_CONTENT_URI, issue.id);
      // Spaltenwerte füllen
      values.put(TimeDataContract.IssueData.Columns.NUMBER, issue.id);
      values.put(TimeDataContract.IssueData.Columns.TITLE, issue.title);
      values.put(TimeDataContract.IssueData.Columns.PRIORITY, issue.priority);
      values.put(TimeDataContract.IssueData.Columns.STATE, issue.state);

      // Speichern
      context.getContentResolver().update(updateUri, values, null, null);
    }
  }
}
