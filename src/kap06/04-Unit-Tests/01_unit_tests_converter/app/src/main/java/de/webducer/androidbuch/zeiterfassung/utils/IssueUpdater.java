package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;

import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class IssueUpdater extends AsyncTask<Void, Void, Void> {
  // Standardrückgabewert
  private final Context _context;

  public IssueUpdater(Context context) {
    _context = context;
  }

  @Override
  protected Void doInBackground(Void... params) {
    try {
      // Client initialisieren
      OkHttpClient client = new OkHttpClient();

      // Anfrage erstellen
      Request request = new Request.Builder()
          .url(IssueHandler.FILTERED_ISSUE_URI)
          .build();

      Response response = client.newCall(request).execute();

      if (!response.isSuccessful()) {
        // Fehler bei der Abfrage der Daten
        return null;
      }

      // Zurückgeben der Daten
      // Parsen mit GSon
      BitbucketIssue[] issues = IssueHandler.parseWithGSon(response.body().string());

      // Speichern in die Datenbank
      IssueHandler.saveToDb(_context, issues);
      return null;
    } catch (IOException e) {
      // Fehler beim Zugriff auf das Internet
      e.printStackTrace();
    }

    return null;
  }
}
