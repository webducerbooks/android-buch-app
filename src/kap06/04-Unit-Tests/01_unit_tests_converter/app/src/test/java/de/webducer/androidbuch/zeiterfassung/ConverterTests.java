package de.webducer.androidbuch.zeiterfassung;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.ParseException;
import java.util.Calendar;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class ConverterTests {
  // Rules
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  // Testmethoden
  @Test
  public void format_WithValidCalendar_ReturnISO8601String() {
    // Arrange
    Calendar input = Calendar.getInstance();
    input.set(2017, 0, 27, 23, 47); // 27.01.2017 23:47
    final String expectedResult = "2017-01-27T23:47";

    // Act
    String result = TimeDataContract.Converter.format(input);

    // Assert
    assertThat(result, is(expectedResult));
  }

  @Test
  public void parse_WithValidISO8601String_ReturnCalendar() throws ParseException {
    // Arrange
    String dbValue = "2017-01-27T23:47";

    // Act
    Calendar result = TimeDataContract.Converter.parse(dbValue);

    // Assert
    assertThat(result.get(Calendar.YEAR), is(2017));
    assertThat(result.get(Calendar.MONTH), is(0)); // "0" Basierte Monatszählung
    assertThat(result.get(Calendar.DAY_OF_MONTH), is(27));
    assertThat(result.get(Calendar.HOUR_OF_DAY), is(23));
    assertThat(result.get(Calendar.MINUTE), is(47));
    assertThat(result.get(Calendar.SECOND), is(0)); // Speichern wir nicht
  }

  @Test
  public void format_WithNullInput_ThrowsNullPointerException(){
    // Arrange
    Calendar input = null;

    // Vorgezogenes Assert
    expectedException.expect(NullPointerException.class);

    // Act
    String result = TimeDataContract.Converter.format(input);
  }

  @Test
  public void parse_WithNullString_ThrowsNullPointerException() throws ParseException {
    // Arrange
    String dbValue = null;

    // Vorgezogenes Assert
    expectedException.expect(NullPointerException.class);

    // Act
    Calendar result = TimeDataContract.Converter.parse(dbValue);
  }

  @Test
  public void parse_WithNotISO8601String_ThrowsParseException() throws ParseException {
    // Arrange
    String dbValue = "2017-01-28 22:47"; // " " statt "T" als Trennzeichen

    // Vorgezogenes Assert
    expectedException.expect(ParseException.class);

    // Act
    Calendar result = TimeDataContract.Converter.parse(dbValue);
  }
}
