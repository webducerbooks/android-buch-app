package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.listener.IChangeDateTimeListener;
import de.webducer.androidbuch.zeiterfassung.listener.IDualogService;

public class ChangeDateModernDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
  // Factory
  public static void showDialog(@NonNull FragmentManager manager, @NonNull Calendar date, int fieldId) {
    // Parameter als Bundle zusammenfassen
    Bundle extras = new Bundle();
    extras.putLong(_DATE_KEY, date.getTimeInMillis());
    extras.putInt(_FIELD_KEY, fieldId);

    // Dialog starten
    ChangeDateModernDialog dialog = new ChangeDateModernDialog();
    dialog.setArguments(extras);
    dialog.show(manager, "DateDialog");
  }

  // Konstanten
  private final static String _DATE_KEY = "DateKey";
  private final static String _FIELD_KEY = "FieldKey";

  // Klaseenvariablen
  private Calendar _date = Calendar.getInstance();
  private int _fieldId;

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IDualogService == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
    }

    // Prüfen, ob Calendar gesetzt ist
    if (getArguments() == null || getArguments().containsKey(_DATE_KEY) == false) {
      throw new NullPointerException("Date is not set");
    }

    // Prüfen, ob Feld-ID gesetzt ist
    if (getArguments().containsKey(_FIELD_KEY) == false) {
      throw new NullPointerException("Field ID is not set");
    }

    // Auslesen der Argumente
    _date.setTimeInMillis(getArguments().getLong(_DATE_KEY));
    _fieldId = getArguments().getInt(_FIELD_KEY);

    // Initialisieren des Dialoges
    DatePickerDialog dialog = new DatePickerDialog(getContext(),
      this, // Änderungscallback
      _date.get(Calendar.YEAR), // Jahr
      _date.get(Calendar.MONTH), // Monat
      _date.get(Calendar.DAY_OF_MONTH)); // Tag

    return dialog;
  }

  @Override
  public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IDualogService == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
    }

    // Interface initialisieren
    IDualogService listener = (IDualogService) getActivity();

    // Datum ändern
    _date.set(year, month, dayOfMonth);

    // Änderung kommunizieren
    listener.notifyDateTimeChanged(_date, _fieldId);
  }
}
