package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.listener.IChangeDateTimeListener;
import de.webducer.androidbuch.zeiterfassung.listener.IDualogService;

public class ChangeTimeModernDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
  // Factory
  public static void showDialog(@NonNull FragmentManager manager, @NonNull Calendar time, int fieldId) {
    // Parameter als Bundle zusammenfassen
    Bundle extras = new Bundle();
    extras.putLong(_TIME_KEY, time.getTimeInMillis());
    extras.putInt(_FIELD_KEY, fieldId);

    // Dialog starten
    ChangeTimeModernDialog dialog = new ChangeTimeModernDialog();
    dialog.setArguments(extras);
    dialog.show(manager, "TimeDialog");
  }

  // Konstanten
  private final static String _TIME_KEY = "TimeKey";
  private final static String _FIELD_KEY = "FieldKey";

  // Klaseenvariablen
  private Calendar _time = Calendar.getInstance();
  private int _fieldId;

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IDualogService == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
    }

    // Prüfen, ob Calendar gesetzt ist
    if (getArguments() == null || getArguments().containsKey(_TIME_KEY) == false) {
      throw new NullPointerException("Date is not set");
    }

    // Prüfen, ob Feld-ID gesetzt ist
    if (getArguments().containsKey(_FIELD_KEY) == false) {
      throw new NullPointerException("Field ID is not set");
    }

    // Auslesen der Argumente
    _time.setTimeInMillis(getArguments().getLong(_TIME_KEY));
    _fieldId = getArguments().getInt(_FIELD_KEY);

    // Einstellung für 24 Stunden Anzeige im Betriebssystem
    boolean is24Format = android.text.format.DateFormat.is24HourFormat(getContext());

    // Initialisieren des Dialoges
    TimePickerDialog dialog = new TimePickerDialog(getContext(),
      this, // Änderungscallback
      _time.get(Calendar.HOUR_OF_DAY), // Stunden
      _time.get(Calendar.MINUTE), // Minuten
      is24Format); // 24 Stunden Anzeige?

    return dialog;
  }

  @Override
  public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    // Interface für Änderungen bestimmen
    if (getActivity() instanceof IDualogService == false) {
      // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
      throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
    }

    // Interface initialisieren
    IDualogService listener = (IDualogService) getActivity();

    // Datum ändern
    _time.set(Calendar.HOUR_OF_DAY, hourOfDay);
    _time.set(Calendar.MINUTE, minute);

    // Änderung kommunizieren
    listener.notifyDateTimeChanged(_time, _fieldId);
  }
}
