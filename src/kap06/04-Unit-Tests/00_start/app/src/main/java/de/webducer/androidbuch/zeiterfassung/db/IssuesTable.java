package de.webducer.androidbuch.zeiterfassung.db;

import android.database.sqlite.SQLiteDatabase;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract.IssueData.Columns;

public class IssuesTable {
  // Konstanten
  /**
   * ID für eine Auflistung
   */
  public static final int ITEM_LIST_ID = 200;

  /**
   * ID für einen Datensatz
   */
  public static final int ITEM_ID = 201;

  /**
   * ID für Datensantz nach Nummer
   */
  public static final int ITEM_BY_NUMBER_ID = 202;

  /**
   * Name der Tabelle
   */
  public static final String TABLE_NAME = "issue_cache";

  // Script für die Erstellung der Tabelle in der Datenbank
  private static final String _CREATE =
      "CREATE TABLE [" + TABLE_NAME + "] ("
          + "[" + Columns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          + "[" + Columns.NUMBER + "] INTEGER NOT NULL,"
          + "[" + Columns.TITLE + "] TEXT,"
          + "[" + Columns.PRIORITY + "] TEXT,"
          + "[" + Columns.STATE + "] TEXT"
          + ")";

  private static final String _MIGRATION_3_TO_4 = "CREATE TABLE [" + TABLE_NAME + "] ("
      + "[" + Columns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
      + "[" + Columns.NUMBER + "] INTEGER NOT NULL,"
      + "[" + Columns.TITLE + "] TEXT,"
      + "[" + Columns.PRIORITY + "] TEXT,"
      + "[" + Columns.STATE + "] TEXT"
      + ")";

  public static void createTable(SQLiteDatabase db) {
    db.execSQL(_CREATE);
  }

  public static void updateTable(SQLiteDatabase db, int oldVersion) {
    switch (oldVersion) {
      case 1:
      case 2:
      case 3:
        // Migration von Version 3 auf 4
        db.execSQL(_MIGRATION_3_TO_4);
    }
  }
}
