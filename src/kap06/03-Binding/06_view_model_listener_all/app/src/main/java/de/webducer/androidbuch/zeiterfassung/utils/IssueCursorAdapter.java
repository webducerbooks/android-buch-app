package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.webducer.androidbuch.zeiterfassung.R;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;

public class IssueCursorAdapter extends SimpleCursorAdapter {
  // Layout für die Zeile
  private static final int _RESSOURCE = R.layout.row_issue_data;

  // Spalten für die Darstellung
  private static final String[] _DISPLAY_COLUMNS = new String[]{
      TimeDataContract.IssueData.Columns.NUMBER,
      TimeDataContract.IssueData.Columns.TITLE,
      TimeDataContract.IssueData.Columns.PRIORITY,
      TimeDataContract.IssueData.Columns.STATE
  };

  // View Zuordnung
  private static final int[] _VIEWS = new int[]{
      R.id.NumberValue,
      R.id.TitleValue,
      R.id.PriorityValue,
      R.id.StateValue
  };

  private int _numberColumnIndex = 0;
  private int _titleColumnIndex = 0;
  private int _priorityColumnIndex = 0;
  private int _stateColumnIndex = 0;

  public IssueCursorAdapter(Context context, int flags) {
    super(context, _RESSOURCE, null, _DISPLAY_COLUMNS, _VIEWS, flags);
  }

  @Override
  public Cursor swapCursor(Cursor c) {
    if (c != null) {
      // Spalten-Index bestimmen
      _numberColumnIndex = c.getColumnIndex(TimeDataContract.IssueData.Columns.NUMBER);
      _titleColumnIndex = c.getColumnIndex(TimeDataContract.IssueData.Columns.TITLE);
      _priorityColumnIndex = c.getColumnIndex(TimeDataContract.IssueData.Columns.PRIORITY);
      _stateColumnIndex = c.getColumnIndex(TimeDataContract.IssueData.Columns.STATE);
    }
    return super.swapCursor(c);
  }

  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    // 1. Holder pattern, um die Elemente nicht jedesmal "suchen" zu müssen,
    // wenn View wiederverwendet wird
    IssueCursorAdapter.IssueViewHolder holder = (IssueCursorAdapter.IssueViewHolder) view.getTag();
    if (holder == null) {
      holder = new IssueCursorAdapter.IssueViewHolder();
      holder.NumberView = (TextView) view.findViewById(R.id.NumberValue);
      holder.TtitleView = (TextView) view.findViewById(R.id.TitleValue);
      holder.PriorityView = (TextView) view.findViewById(R.id.PriorityValue);
      holder.StateView = (TextView) view.findViewById(R.id.StateValue);
      view.setTag(holder);
    }

    // 2. Inhalte in View einfügen
    // - Number
    if (cursor.isNull(_numberColumnIndex)) {
      // Wert auf "" setzen
      holder.NumberView.setText("");
    } else {
      // Wert aus der Datenbank setzen
      holder.NumberView.setText(String.valueOf(cursor.getInt(_numberColumnIndex)));
    }

    // - Title
    if (cursor.isNull(_titleColumnIndex)) {
      // Wert auf "" setzen
      holder.TtitleView.setText("");
    } else {
      // Wert aus der Datenbank setzen
      holder.TtitleView.setText(cursor.getString(_titleColumnIndex));
    }

    // - Priority
    if (cursor.isNull(_priorityColumnIndex)) {
      // Wert auf "" setzen
      holder.PriorityView.setText("");
    } else {
      // Wert aus der Datenbank setzen
      holder.PriorityView.setText(cursor.getString(_priorityColumnIndex));
    }

    // - State
    if (cursor.isNull(_stateColumnIndex)) {
      // Wert auf "" setzen
      holder.StateView.setText("");
    } else {
      // Wert aus der Datenbank setzen
      holder.StateView.setText(cursor.getString(_stateColumnIndex));
    }
  }

  final class IssueViewHolder {
    public TextView NumberView;
    public TextView TtitleView;
    public TextView StateView;
    public TextView PriorityView;
  }
}
