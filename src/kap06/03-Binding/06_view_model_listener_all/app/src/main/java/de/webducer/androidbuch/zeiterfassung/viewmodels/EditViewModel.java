package de.webducer.androidbuch.zeiterfassung.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.BR;
import de.webducer.androidbuch.zeiterfassung.listener.IDualogService;

public class EditViewModel extends BaseObservable {
  private String _comment;
  private int _pause = 0;
  private Calendar _startTime = Calendar.getInstance();
  private Calendar _endTime = Calendar.getInstance();
  private IDualogService _dialogService;

  @Bindable
  public String getComment() {
    return _comment;
  }

  public void setComment(String comment) {
    if (_comment == null && comment == null) {
      // Keine Änderung, beide sind NULL
      return;
    }

    if (_comment != null && comment != null && _comment.equals(comment)) {
      // Beide haben denselben Inhalt, keine Änderung
      return;
    }

    // Änderung vorhanden, festhalten und benachrichtigen
    _comment = comment;
    notifyPropertyChanged(BR.comment);
  }

  @Bindable
  public int getPause() {
    return _pause;
  }

  public void setPause(int pause) {
    if (_pause == pause) {
      // Werte sind gleich, keine Änderung
      return;
    }

    _pause = pause;
    notifyPropertyChanged(BR.pause);
  }

  @Bindable
  public Calendar getStartTime() {
    return _startTime;
  }

  public void setStartTime(Calendar startTime) {
    if (_startTime == null && startTime == null) {
      // Beide sind NULL
      return;
    }

    if (_startTime != null && startTime != null && _startTime.equals(startTime)) {
      // Beide sind gleich
      return;
    }

    _startTime = startTime;
    notifyPropertyChanged(BR.startTime);
  }

  @Bindable
  public Calendar getEndTime() {
    return _endTime;
  }

  public void setEndTime(Calendar endTime) {
    if (_endTime == null && endTime == null) {
      // Beide sind NULL
      return;
    }

    if (_endTime != null && endTime != null && _endTime.equals(endTime)) {
      // Beide sind gleich
      return;
    }

    _endTime = endTime;
    notifyPropertyChanged(BR.endTime);
  }

  public void setDialogService(IDualogService dialogService) {
    _dialogService = dialogService;
  }

  public boolean changeStartDate() {
    // Dialog zur Änderung über Service starten, falls dieser zugeordnet wurde
    if (_dialogService != null) {
      _dialogService.changeDate(_startTime, BR.startTime);
    }

    return true;
  }

  public boolean changeEndDate() {
    // Dialog zur Änderung über Service starten, falls dieser zugeordnet wurde
    if (_dialogService != null) {
      _dialogService.changeDate(_endTime, BR.endTime);
    }

    return true;
  }

  public boolean changeStartTime() {
    // Dialog zur Änderung über Service starten, falls dieser zugeordnet wurde
    if (_dialogService != null) {
      _dialogService.changeTime(_startTime, BR.startTime);
    }

    return true;
  }

  public boolean changeEndTime() {
    // Dialog zur Änderung über Service starten, falls dieser zugeordnet wurde
    if (_dialogService != null) {
      _dialogService.changeTime(_endTime, BR.endTime);
    }

    return true;
  }

  public void notifyDateChenaged(@NonNull Calendar newDate, int fieldId) {
    // Setzen der neuen Startzeit
    if (fieldId == BR.startTime) {
      setStartTime(newDate);
    }
    // Setzen der neuen Endzeit
    else if (fieldId == BR.endTime) {
      setEndTime(newDate);
    }
  }
}
