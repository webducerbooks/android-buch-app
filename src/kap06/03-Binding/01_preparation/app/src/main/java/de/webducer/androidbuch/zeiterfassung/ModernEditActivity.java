package de.webducer.androidbuch.zeiterfassung;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityModernEditBinding;

public class ModernEditActivity extends AppCompatActivity {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Bindung initialisieren
    ActivityModernEditBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_modern_edit);
  }
}
