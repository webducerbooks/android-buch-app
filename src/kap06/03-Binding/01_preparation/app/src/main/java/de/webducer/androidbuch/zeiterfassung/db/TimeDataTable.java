package de.webducer.androidbuch.zeiterfassung.db;

import android.database.sqlite.SQLiteDatabase;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract.TimeData.Columns;

public class TimeDataTable {
  // Konstanten
  /**
   * ID für eine Auflistung
   */
  public static final int ITEM_LIST_ID = 100;

  /**
   * ID für einen Datensatz
   */
  public static final int ITEM_ID = 101;

  /**
   * ID für einen Datensatz, das noch nicht beendet wurde
   */
  public static final int NOT_FINISHED_ITEM_ID = 102;

  /**
   * Name der Tabelle
   */
  public static final String TABLE_NAME = "time_data";

  // Script für die Erstellung der Tabelle in der Datenbank
  private static final String _CREATE =
    "CREATE TABLE [" + TABLE_NAME + "] ("
      + "[" + Columns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
      + "[" + Columns.START_TIME + "] TEXT NOT NULL,"
      + "[" + Columns.END_TIME + "] TEXT,"
      + "[" + Columns.PAUSE + "] INTEGER NOT NULL DEFAULT 0,"
      + "[" + Columns.COMMENT + "] TEXT"
      + ")";

  /* Scripte für die Migrationen */
  // Migration von Version 1 auf 2
  private static final String _MIGRATION_1_TO_2 =
    "ALTER TABLE [" + TABLE_NAME + "] "
      + "ADD COLUMN [" + Columns.PAUSE + "] INTEGER NOT NULL DEFAULT 0";
  // Migration von Version 2 auf 3
  private static final String _MIGRATION_2_TO_3 =
    "ALTER TABLE [" + TABLE_NAME + "] "
      + "ADD COLUMN [" + Columns.COMMENT + "] TEXT";

  public static void createTable(SQLiteDatabase db) {
    db.execSQL(_CREATE);
  }

  public static void updateTable(SQLiteDatabase db, int oldVersion) {
    switch (oldVersion) {
      case 1:
        // Migration von Version 1 auf 2
        db.execSQL(_MIGRATION_1_TO_2);

      case 2:
        // migration von Version 2 auf 3
        db.execSQL(_MIGRATION_2_TO_3);
    }
  }
}
