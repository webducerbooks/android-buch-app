package de.webducer.androidbuch.zeiterfassung;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  private DateFormat _dateTimeFormatter = DateFormat.getDateTimeInstance(
    DateFormat.SHORT, DateFormat.SHORT);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Eingabefeld für Startzeit suchen
    _startDateTime = (EditText) findViewById(R.id.StartDateTime);
    _endDateTime = (EditText) findViewById(R.id.EndDateTime);
    _startCommand = (Button) findViewById(R.id.StartCommand);
    _endCommand = (Button) findViewById(R.id.EndCommand);

    // Tastatureingaben verhindern
    _startDateTime.setKeyListener(null);
    _endDateTime.setKeyListener(null);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Initialisierung aus der Datenbank
    initFromDb();
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Button Klick registrieren
    _startCommand.setOnClickListener(new StartButtonClicked());
    _endCommand.setOnClickListener(new EndButtonClicked());
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Buttin Klick deregisitrieren
    _startDateTime.setOnClickListener(null);
    _endDateTime.setOnClickListener(null);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Deserialisieren des Menüs aus XML
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Verarbeiten dem Menüklicks
    switch (item.getItemId()) {
      case R.id.ListDataMenuItem:
        // Expliziter Intent
        Intent listDataIntent = new Intent(this, ListDataActivity.class);
        startActivity(listDataIntent);
        return true;

      case R.id.InfoMenuItem:
        // Expliziter Intent für die Info Seite
        Intent infoIntent = new Intent(this, InfoActivity.class);
        startActivity(infoIntent);
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }

  }

  // Initialisierung der Daten aus der Datenbank
  private void initFromDb() {
    // Deaktivieren der Buttons
    _startCommand.setEnabled(false);
    _endCommand.setEnabled(false);

    // Laden des offenen Datensatzes, falls vorhanden
    Cursor data = getContentResolver().query(
      TimeDataContract.TimeData.NOT_FINISHED_CONTENT_URI,
      new String[]{TimeDataContract.TimeData.Columns.START_TIME},
      null, // Keine Bedingungen
      null, // Keine Argumente
      null); // Keine Sortierung

    // Prüfen, ob Daten vorhanden sind
    if (data.moveToFirst()) {
      try {
        // mindestens ein Datensatz vorhanden
        Calendar startTime = TimeDataContract.Converter.parse(data.getString(0));
        _startDateTime.setText(_dateTimeFormatter.format(startTime.getTime()));
      } catch (ParseException e) {
        // Fehler bei der Konvertierung der Startzeit
        _startDateTime.setText("Falscher Datumsformat in der Datenbank");
      }
      // Beenden Button aktivieren
      _endDateTime.setText("");
      _endCommand.setEnabled(true);
    } else {
      // Start Button aktivieren
      _startDateTime.setText("");
      _endDateTime.setText("");
      _startCommand.setEnabled(true);
    }

    // Schließen, da die Daten nicht weiter benutzt werden
    data.close();
  }

  // Klasse für die Behandlung des Klicks auf den Start-Button
  class StartButtonClicked implements View.OnClickListener {

    @Override
    public void onClick(View v) {
      // Start Button deaktivieren
      _startCommand.setEnabled(false);

      // Aktuelles Datum und Uhrzeit
      Calendar currentTime = Calendar.getInstance();
      // Formatierung in ISO 8601
      String dbCurrentTime =
        TimeDataContract.Converter.format(currentTime);
      // Zuordnen der Zeit der Startzeit Spalte
      ContentValues values = new ContentValues();
      values.put(
        TimeDataContract.TimeData.Columns.START_TIME,
        dbCurrentTime);
      // Einfügen des Datensatzen in die Datenbank
      getContentResolver()
        .insert(TimeDataContract.TimeData.CONTENT_URI, values);
      // Ausgabe des aktuellen Datums auf der Oberfläche
      _startDateTime.setText(
        _dateTimeFormatter.format(currentTime.getTime()));

      // Beenden Button aktivieren
      _endCommand.setEnabled(true);
    }
  }

  // Klasse für die Behandlung des Klicks auf den Beenden-Button
  class EndButtonClicked implements View.OnClickListener {

    @Override
    public void onClick(View v) {
      // Beenden Button deaktivieren
      _endCommand.setEnabled(false);

      // Aktuelles Datum und Uhrzeit
      Calendar currentTime = Calendar.getInstance();
      // Formatierung in ISO 8601
      String dbCurrentTime = TimeDataContract.Converter.format(currentTime);
      // Zuordnen der Zeit dem Endzeit Spalte
      ContentValues values = new ContentValues();
      values.put(TimeDataContract.TimeData.Columns.END_TIME, dbCurrentTime);
      // Aktualisieren des offenen Datensatzes in der Datenbank
      getContentResolver().update(TimeDataContract.TimeData.NOT_FINISHED_CONTENT_URI,
        values, null, null);
      // Ausgabe des aktuellen Datums auf der Oberfläche
      _endDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));

      // Start Button aktivieren
      _startCommand.setEnabled(true);
    }
  }
}
