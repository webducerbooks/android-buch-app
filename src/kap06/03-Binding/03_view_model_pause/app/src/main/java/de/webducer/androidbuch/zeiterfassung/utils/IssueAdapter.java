package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import de.webducer.androidbuch.zeiterfassung.R;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;

public class IssueAdapter extends ArrayAdapter<BitbucketIssue> {
  @NonNull
  private final Context _context;
  // Layout für die Zeile
  private static final int _resource = R.layout.row_issue_data;

  public IssueAdapter(@NonNull Context context) {
    super(context, _resource);
    _context = context;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    // 1. View initialisieren, falls notwendig
    if (convertView == null) {
      convertView = View.inflate(_context, _resource, null);
    }

    // 2. Holder pattern, um die Elemente nicht jedesmal "suchen" zu müssen,
    // wenn View wiederverwendet wird
    IssueViewHolder holder = (IssueViewHolder) convertView.getTag();
    if (holder == null) {
      holder = new IssueViewHolder();
      holder.NumberView = (TextView) convertView.findViewById(R.id.NumberValue);
      holder.TtitleView = (TextView) convertView.findViewById(R.id.TitleValue);
      holder.PriorityView = (TextView) convertView.findViewById(R.id.PriorityValue);
      holder.StateView = (TextView) convertView.findViewById(R.id.StateValue);
      convertView.setTag(holder);
    }

    // 3. Aktuell zu füllenden Inhalt holen
    BitbucketIssue issue = getItem(position);

    // 4. Inhalte in View einfügen
    holder.NumberView.setText(String.valueOf(issue.id));
    holder.TtitleView.setText(issue.title);
    holder.StateView.setText(issue.state);
    holder.PriorityView.setText(issue.priority);

    return convertView;
  }

  final class IssueViewHolder {
    public TextView NumberView;
    public TextView TtitleView;
    public TextView StateView;
    public TextView PriorityView;
  }
}
