package de.webducer.androidbuch.zeiterfassung.models;

public class BitbucketIssue {
  public int id;
  public String title;
  public String priority;
  public String state;
  public BitbucketIssueContent content;
}
