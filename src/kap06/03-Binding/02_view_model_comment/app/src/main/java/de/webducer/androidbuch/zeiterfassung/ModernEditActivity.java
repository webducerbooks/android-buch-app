package de.webducer.androidbuch.zeiterfassung;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityModernEditBinding;
import de.webducer.androidbuch.zeiterfassung.viewmodels.EditViewModel;

public class ModernEditActivity extends AppCompatActivity {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";

  // View Models
  private EditViewModel _viewModel = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Bindung initialisieren
    ActivityModernEditBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_modern_edit);
    // View Model initialisieren
    _viewModel = new EditViewModel();
    // View Model mit Layout verbinden
    binding.setEditData(_viewModel);
  }

  @Override
  protected void onStart() {
    super.onStart();

    _viewModel.setComment("Hallo binding World!");
  }
}
