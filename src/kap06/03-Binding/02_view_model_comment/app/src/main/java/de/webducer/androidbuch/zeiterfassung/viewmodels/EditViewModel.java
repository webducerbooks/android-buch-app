package de.webducer.androidbuch.zeiterfassung.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import de.webducer.androidbuch.zeiterfassung.BR;

public class EditViewModel extends BaseObservable {
  private String _comment;

  @Bindable
  public String getComment() {
    return _comment;
  }

  public void setComment(String comment) {
    if (_comment == null && comment == null) {
      // Keine Änderung, beide sind NULL
      return;
    }

    if (_comment != null && comment != null && _comment.equals(comment)) {
      // Beide haben denselben Inhalt, keine Änderung
      return;
    }

    // Änderung vorhanden, festhalten und benachrichtigen
    _comment = comment;
    notifyPropertyChanged(BR.comment);
  }
}
