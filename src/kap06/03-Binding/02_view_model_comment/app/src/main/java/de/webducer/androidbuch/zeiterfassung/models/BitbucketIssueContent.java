package de.webducer.androidbuch.zeiterfassung.models;

public class BitbucketIssueContent {
  public String html;
  public String raw;
}
