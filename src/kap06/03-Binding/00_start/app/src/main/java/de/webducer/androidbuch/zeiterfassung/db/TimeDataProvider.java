package de.webducer.androidbuch.zeiterfassung.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.sql.PreparedStatement;
import java.util.Locale;

public class TimeDataProvider extends ContentProvider {
  // Klassenvariablen
  private static final UriMatcher _URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

  // Klasseninstanz für die Datenbankhilfsklasse
  private DbHelper _dbHelper = null;

  // Allgemeiner Filter für ID
  private static final String _ID_WHERE = BaseColumns._ID + "=?";

  // Filter für den nicht beendeten Datensatz
  private static final String _NOT_FINISHED_WHERE = "IFNULL("
      + TimeDataContract.TimeData.Columns.END_TIME
      + ",'')=''";

  // Filter nach Nummer
  private static final String _BY_NUMBER_WHERE = TimeDataContract.IssueData.Columns.NUMBER + "=?";

  static {
    /* Time Data */
    // Lookup für die Auflistung
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis-Uri
        TimeDataContract.TimeData.CONTENT_DIRECTORY, // Unterverzeichnis der Daten
        TimeDataTable.ITEM_LIST_ID); // Eindeutige ID

    // Lookup für ein Datensatz
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis-Uri
        TimeDataContract.TimeData.CONTENT_DIRECTORY + "/#", // Unterverzeichnis mit ID des Datensatzes
        TimeDataTable.ITEM_ID); // Eindeutige ID

    // Lookup für den offenen Datensatz
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis Uri
        TimeDataContract.TimeData.NOT_FINISHED_CONTENT_DIRECTORY, // Unterverzeichnis
        TimeDataTable.NOT_FINISHED_ITEM_ID); // ID für offenen Datensatz

    /* Issue Data */
    // Lookup für die Auflistung
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis-Uri
        TimeDataContract.IssueData.CONTENT_DIRECTORY, // Unterverzeichnis der Daten
        IssuesTable.ITEM_LIST_ID); // Eindeutige ID

    // Lookup für ein Datensatz
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis-Uri
        TimeDataContract.IssueData.CONTENT_DIRECTORY + "/#", // Unterverzeichnis mit ID des Datensatzes
        IssuesTable.ITEM_ID); // Eindeutige ID

    // Lookup für ein Datensatz nach Nummer
    _URI_MATCHER.addURI(
        TimeDataContract.AUTHORITY, // Basis-Uri
        TimeDataContract.IssueData.BY_NUMBER_CONTENT_DIRECTORY + "/#", // Unterverzeichnis mit ID des Datensatzes
        IssuesTable.ITEM_BY_NUMBER_ID); // Eindeutige ID
  }

  @Override
  public boolean onCreate() {
    _dbHelper = new DbHelper(getContext());
    return true;
  }

  @Nullable
  @Override
  public String getType(@NonNull Uri uri) {
    // Auflösen der Uri
    final int uriType = _URI_MATCHER.match(uri);
    String type = null;

    // Bestimmen des Datentyps
    switch (uriType) {
      case TimeDataTable.ITEM_LIST_ID:
        type = TimeDataContract.TimeData.CONTENT_TYPE;
        break;

      case TimeDataTable.ITEM_ID:
      case TimeDataTable.NOT_FINISHED_ITEM_ID:
        type = TimeDataContract.TimeData.CONTENT_ITEM_TYPE;
        break;

      case IssuesTable.ITEM_LIST_ID:
        type = TimeDataContract.IssueData.CONTENT_TYPE;
        break;

      case IssuesTable.ITEM_ID:
      case IssuesTable.ITEM_BY_NUMBER_ID:
        type = TimeDataContract.IssueData.CONTENT_ITEM_TYPE;
        break;
    }

    return type;
  }

  @Nullable
  @Override
  public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
    // Auflösen der Uri
    final int uriType = _URI_MATCHER.match(uri);
    Uri insertUri = null;
    long newItemId;
    SQLiteDatabase db = _dbHelper.getWritableDatabase();

    // Bestimmen der Aktionen für die gefundene Uri
    switch (uriType) {
      case TimeDataTable.ITEM_LIST_ID:
      case TimeDataTable.ITEM_ID:
      case TimeDataTable.NOT_FINISHED_ITEM_ID:
        newItemId = db.insert(TimeDataTable.TABLE_NAME, null, values);
        break;

      case IssuesTable.ITEM_LIST_ID:
      case IssuesTable.ITEM_ID:
      case IssuesTable.ITEM_BY_NUMBER_ID:
        newItemId = db.insert(IssuesTable.TABLE_NAME, null, values);
        break;

      default:
        // Ausnahme erzeugen, da wir die Uri nicht kennen
        throw new IllegalArgumentException(String.format(Locale.GERMANY, "Unbekannte URI: %s", uri));
    }

    // Datensatz erfogreich hinzugefügt
    if (newItemId > 0) {
      // Erstellen der Uri
      insertUri = ContentUris.withAppendedId(
          TimeDataContract.TimeData.CONTENT_URI, // Basis-Uri für die Daten
          newItemId); // ID des Datensatzes

      // Benachrichtigen über die Änderungen der Daten
      getContext().getContentResolver().notifyChange(uri, null);
    }

    return insertUri;
  }

  @Nullable
  @Override
  public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
    final int uriType = _URI_MATCHER.match(uri);
    Cursor data;
    SQLiteDatabase db = _dbHelper.getReadableDatabase();

    switch (uriType) {
      case TimeDataTable.ITEM_LIST_ID:
        data = db.query(TimeDataTable.TABLE_NAME, // Tabelle
            projection, // Spalten
            selection, // Filter
            selectionArgs, // Filter-Argumente
            null, // Gruppierung
            null, // Having für Gruppierung
            sortOrder); // Sortierung
        break;

      case TimeDataTable.ITEM_ID:
        // Extrahieren der ID aus der URI
        final long id = ContentUris.parseId(uri);
        // ID als String-Array verpacken
        final String[] idArgs = new String[]{String.valueOf(id)};
        // Abfragen des Datensatzes
        data = db.query(TimeDataTable.TABLE_NAME, projection, _ID_WHERE, idArgs,
            null, null, null);
        break;

      case TimeDataTable.NOT_FINISHED_ITEM_ID:
        data = db.query(TimeDataTable.TABLE_NAME, projection, _NOT_FINISHED_WHERE, null,
            null, null, null);
        break;

      case IssuesTable.ITEM_LIST_ID:
        data = db.query(IssuesTable.TABLE_NAME, projection, selection, selectionArgs,
            null, null, sortOrder);
        break;

      case IssuesTable.ITEM_ID:
        final long issueId = ContentUris.parseId(uri);
        final String[] issueArgs = new String[]{String.valueOf(issueId)};
        data = db.query(IssuesTable.TABLE_NAME, projection, _ID_WHERE, issueArgs,
            null, null, null);
        break;

      case IssuesTable.ITEM_BY_NUMBER_ID:
        final long issueNumber = ContentUris.parseId(uri);
        final String[] issueNumberArgs = new String[]{String.valueOf(issueNumber)};
        data = db.query(IssuesTable.TABLE_NAME, projection, _BY_NUMBER_WHERE, issueNumberArgs,
            null, null, null);
        break;

      default:
        // Ausnahme erzeugen, da wir die Uri nicht kennen
        throw new IllegalArgumentException(String.format(Locale.GERMANY, "Unbekannte URI: %s", uri));
    }

    // Datensätze erfolgreich gelesen
    if (data != null) {
      // Benachrichtigung über Änderungen erhalten
      data.setNotificationUri(getContext().getContentResolver(), uri);
    }

    return data;
  }

  @Override
  public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
    final int uriType = _URI_MATCHER.match(uri);
    int deletedItems = 0;
    SQLiteDatabase db = _dbHelper.getWritableDatabase();

    // Bestimmen der Aktionen, abhängig vom Typ
    switch (uriType) {
      case TimeDataTable.ITEM_LIST_ID:
        deletedItems = db.delete(
            TimeDataTable.TABLE_NAME, // Tabellenname
            selection, // Filter
            selectionArgs); // Filterargumente
        break;

      case TimeDataTable.ITEM_ID:
        // Extrahieren der ID aus der URI
        final long id = ContentUris.parseId(uri);
        // ID als String-Array verpacken
        final String[] idArgs = new String[]{String.valueOf(id)};
        // Löschen von einen einzigen Datensatz
        deletedItems = db.delete(TimeDataTable.TABLE_NAME, _ID_WHERE, idArgs);
        break;

      case TimeDataTable.NOT_FINISHED_ITEM_ID:
        deletedItems = db.delete(TimeDataTable.TABLE_NAME, _NOT_FINISHED_WHERE, null);
        break;

      case IssuesTable.ITEM_LIST_ID:
        deletedItems = db.delete(
            IssuesTable.TABLE_NAME, // Tabellenname
            selection, // Filter
            selectionArgs); // Filterargumente
        break;

      case IssuesTable.ITEM_ID:
        // Extrahieren der ID aus der URI
        final long issueId = ContentUris.parseId(uri);
        // ID als String-Array verpacken
        final String[] issueArgs = new String[]{String.valueOf(issueId)};
        // Löschen von einen einzigen Datensatz
        deletedItems = db.delete(IssuesTable.TABLE_NAME, _ID_WHERE, issueArgs);
        break;

      case IssuesTable.ITEM_BY_NUMBER_ID:
        final long issueNumber = ContentUris.parseId(uri);
        final String[] issueNumberArgs = new String[]{String.valueOf(issueNumber)};
        deletedItems = db.delete(IssuesTable.TABLE_NAME, _BY_NUMBER_WHERE, issueNumberArgs);
        break;

      default:
        // Ausnahme erzeugen, da wir die Uri nicht kennen
        throw new IllegalArgumentException(String.format(Locale.GERMANY, "Unbekannte URI: %s", uri));
    }

    // Datensätze erfolgreich gelöscht
    if (deletedItems > 0) {
      // Benachrichtigung über die Änderungen der Daten
      getContext().getContentResolver().notifyChange(uri, null);
    }

    return deletedItems;
  }

  @Override
  public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
    final int uriType = _URI_MATCHER.match(uri);
    int updateItems = 0;
    SQLiteDatabase db = _dbHelper.getWritableDatabase();

    switch (uriType) {
      case TimeDataTable.ITEM_LIST_ID:
        updateItems = db.update(
            TimeDataTable.TABLE_NAME, // Tabellenname
            values, // Spalten mit Werten für die Aktualisierung
            selection, // Filter
            selectionArgs); // Filterargumente
        break;

      case TimeDataTable.ITEM_ID:
        // Extrahieren der ID aus der URI
        final long id = ContentUris.parseId(uri);
        // ID als String-Array verpacken
        final String[] idArgs = new String[]{String.valueOf(id)};
        // Aktualisieren von einen Datensatz
        updateItems = db.update(TimeDataTable.TABLE_NAME, values, _ID_WHERE, idArgs);
        break;

      case TimeDataTable.NOT_FINISHED_ITEM_ID:
        updateItems = db.update(TimeDataTable.TABLE_NAME, values, _NOT_FINISHED_WHERE, null);
        break;

      case IssuesTable.ITEM_LIST_ID:
        updateItems = db.update(
            IssuesTable.TABLE_NAME, // Tabellenname
            values, // Spalten mit Werten für die Aktualisierung
            selection, // Filter
            selectionArgs); // Filterargumente
        break;

      case IssuesTable.ITEM_ID:
        // Extrahieren der ID aus der URI
        final long issueId = ContentUris.parseId(uri);
        // ID als String-Array verpacken
        final String[] issueArgs = new String[]{String.valueOf(issueId)};
        // Aktualisieren von einen Datensatz
        updateItems = db.update(IssuesTable.TABLE_NAME, values, _ID_WHERE, issueArgs);
        break;

      case IssuesTable.ITEM_BY_NUMBER_ID:
        final long issueNumber = ContentUris.parseId(uri);
        final String[] issueNumberArgs = new String[]{String.valueOf(issueNumber)};
        // Prüfen, ob Datensatz vorhanden ist
        SQLiteStatement stmInsert = db.compileStatement("SELECT COUNT(*) FROM "
            + IssuesTable.TABLE_NAME + " WHERE "
            + TimeDataContract.IssueData.Columns.NUMBER + "=?");
        stmInsert.bindLong(1, issueNumber);
        if (stmInsert.simpleQueryForLong() == 1L) {
          // Datensatz vorhanden => Update
          values.remove(TimeDataContract.IssueData.Columns.NUMBER);
          updateItems = db.update(IssuesTable.TABLE_NAME, values, _BY_NUMBER_WHERE, issueNumberArgs);
        } else {
          // Neuer Datensatz
          values.put(TimeDataContract.IssueData.Columns.NUMBER, issueNumber);
          db.insert(IssuesTable.TABLE_NAME, null, values);
          updateItems = 1;
        }
        break;

      default:
        // Ausnahme erzeugen, da wir die Uri nicht kennen
        throw new IllegalArgumentException(String.format(Locale.GERMANY, "Unbekannte URI: %s", uri));
    }

    // Datensätze erfolgreich aktualisiert
    if (updateItems > 0) {
      // Benachrichtigung über die Änderungen der Daten
      getContext().getContentResolver().notifyChange(uri, null);
    }

    return updateItems;
  }
}
