package de.webducer.androidbuch.zeiterfassung.bindingadapter;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

public class CalendarBindingAdapter {
  @BindingAdapter("bind:calendarTime")
  public static void setCelandarAsTime(TextView view, Calendar dateTime) {
    String newValue = "";
    if (dateTime != null) {
      // Formatter initialisieren
      DateFormat timeFormatter = android.text.format.DateFormat.getTimeFormat(view.getContext());

      // Formatierten Wert ausgeben, wenn dieser sich geändert hat
      newValue = timeFormatter.format(dateTime.getTime());
    }

    // Prüfen, ob die Werte gleich sind
    if (newValue.equals(view.getText().toString())) {
      return;
    }

    // Neuen Wert setzen
    view.setText(newValue);
  }

  @BindingAdapter("bind:calendarDate")
  public static void setCelandarAsDate(TextView view, Calendar dateTime) {
    String newValue = "";
    if (dateTime != null) {
      // Formatter initialisieren
      DateFormat dateFormatter = android.text.format.DateFormat.getDateFormat(view.getContext());

      // Formatierten Wert ausgeben, wenn dieser sich geändert hat
      newValue = dateFormatter.format(dateTime.getTime());
    }

    // Prüfen, ob die Werte gleich sind
    if (newValue.equals(view.getText().toString())) {
      return;
    }

    // Neuen Wert setzen
    view.setText(newValue);
  }
}
