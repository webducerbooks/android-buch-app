package de.webducer.androidbuch.zeiterfassung;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityModernEditBinding;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateModernDialog;
import de.webducer.androidbuch.zeiterfassung.listener.IDualogService;
import de.webducer.androidbuch.zeiterfassung.viewmodels.EditViewModel;

public class ModernEditActivity extends AppCompatActivity implements IDualogService {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";

  // View Models
  private EditViewModel _viewModel = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Bindung initialisieren
    ActivityModernEditBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_modern_edit);
    // View Model initialisieren
    _viewModel = new EditViewModel();
    // View Model mit Layout verbinden
    binding.setEditData(_viewModel);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Dialog Service setzen
    _viewModel.setDialogService(this);

    _viewModel.setComment("Hallo binding World!");
    _viewModel.setPause(30);
  }

  @Override
  protected void onStop() {
    super.onStop();

    // Dialogservice wieder entziehen
    _viewModel.setDialogService(null);
  }

  @Override
  public void changeDate(@NonNull Calendar originDate, int fieldId) {
    // Dialog starten
    ChangeDateModernDialog.showDialog(getSupportFragmentManager(), originDate, fieldId);
  }

  @Override
  public void changeTime(@NonNull Calendar originTime, int fieldId) {

  }

  @Override
  public void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldId) {
    // View Model benachrichtigen
    _viewModel.notifyDateChenaged(newDateTime, fieldId);
  }
}
