package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ProviderTestCase2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataProvider;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class TimeDataProviderTests extends ProviderTestCase2<TimeDataProvider> {
  // Standardkonstruktor mit den Werten unseres Providers
  public TimeDataProviderTests() {
    super(TimeDataProvider.class, TimeDataContract.AUTHORITY);
  }

  // Initialisierung und Aufräumen aus JUnit3 anstossen
  @Before
  public void onBeforeTest() throws Exception {
    setContext(InstrumentationRegistry.getTargetContext());
    super.setUp();
  }

  @After
  public void onAfterTest() throws Exception {
    super.tearDown();
  }

  @Test
  public void initialDb_HasNoEntries() {
    // Arrange

    // Act
    Cursor data = getMockContentResolver().query(
      TimeDataContract.TimeData.CONTENT_URI,
      null,
      null,
      null,
      null);

    // Assert
    assertThat(data, is(not(nullValue())));
    assertThat(data.getCount(), is(0));
  }

  @Test
  public void addNewEntry_StoresInDb_WithGivenData(){
    // Arrange
    final String startDate = "2017-05-31T22:47";
    final String endDate = "2017-06-01T07:29";
    final int pause = 40;
    final String comment = "Work at night";
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.START_TIME, startDate);
    values.put(TimeDataContract.TimeData.Columns.END_TIME, endDate);
    values.put(TimeDataContract.TimeData.Columns.PAUSE, pause);
    values.put(TimeDataContract.TimeData.Columns.COMMENT, comment);
    final String[] columns = {
      TimeDataContract.TimeData.Columns._ID,
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME,
      TimeDataContract.TimeData.Columns.PAUSE,
      TimeDataContract.TimeData.Columns.COMMENT
    };

    // Act
    Uri insertUri = getMockContentResolver().insert(
      TimeDataContract.TimeData.CONTENT_URI,
      values);

    // Assert
    Cursor insertedData = getMockContentResolver().query(
      insertUri, columns, null, null, null);
    Cursor allData = getMockContentResolver().query(
      TimeDataContract.TimeData.CONTENT_URI,
      null, null, null, null);
    assertThat(allData.getCount(), is(1)); // Nur 1 Datensatz da
    assertThat(insertedData.getCount(), is(1)); // Uri geht auf neuen Datensatz
    insertedData.moveToFirst(); // Ersten Datensatz ansteuern
    assertThat(insertedData.getString(1), is(startDate)); // Wert in 2. Spalte
    assertThat(insertedData.getString(2), is(endDate)); // Wert in 3. Spalte
    assertThat(insertedData.getInt(3), is(pause)); // Wert in 4. Spalte
    assertThat(insertedData.getString(4), is(comment)); // Wert in 5. Spalte
  }

  @Test
  public void updateEntry_UpdateInDb_WithGivenData(){
    // Arrange
    createNewEntry();
    long updateId = createNewEntry();
    final String startDate = "2017-06-05T09:30";
    final String endDate = "2017-06-05T17:25";
    final int pause = 55;
    final String comment = "New job";
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.START_TIME, startDate);
    values.put(TimeDataContract.TimeData.Columns.END_TIME, endDate);
    values.put(TimeDataContract.TimeData.Columns.PAUSE, pause);
    values.put(TimeDataContract.TimeData.Columns.COMMENT, comment);
    final String[] columns = {
      TimeDataContract.TimeData.Columns._ID,
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME,
      TimeDataContract.TimeData.Columns.PAUSE,
      TimeDataContract.TimeData.Columns.COMMENT
    };
    Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, updateId);

    // Act
    int updateCount = getMockContentResolver().update(
      updateUri,
      values, null, null);

    // Assert
    assertThat(updateCount, is(1)); // Nur ein Update durchgeführt
    Cursor insertedData = getMockContentResolver().query(
      updateUri, columns, null, null, null);
    Cursor allData = getMockContentResolver().query(
      TimeDataContract.TimeData.CONTENT_URI,
      null, null, null, null);
    assertThat(allData.getCount(), is(2)); // Nur 2 Datensätze da
    assertThat(insertedData.getCount(), is(1)); // Uri geht auf neuen Datensatz
    insertedData.moveToFirst(); // Ersten Datensatz ansteuern
    assertThat(insertedData.getString(1), is(startDate)); // Wert in 2. Spalte
    assertThat(insertedData.getString(2), is(endDate)); // Wert in 3. Spalte
    assertThat(insertedData.getInt(3), is(pause)); // Wert in 4. Spalte
    assertThat(insertedData.getString(4), is(comment)); // Wert in 5. Spalte
  }

  @Test
  public void checkAllTimeDataColumns_AgainstContract(){
    // Arrange

    // Act
    String[] columns = getMockContentResolver().query(
      TimeDataContract.TimeData.CONTENT_URI,
      null,
      TimeDataContract.TimeData.Columns._ID + "=?",
      new String[]{"1"},
      null)
      .getColumnNames();

    // Assert
    for (String columnName : columns) {
      switch (columnName){
        case TimeDataContract.TimeData.Columns._ID:
        case TimeDataContract.TimeData.Columns.START_TIME:
        case TimeDataContract.TimeData.Columns.END_TIME:
        case TimeDataContract.TimeData.Columns.PAUSE:
        case TimeDataContract.TimeData.Columns.COMMENT:
          // Diese Spalten werden erwartet
          break;

        default:
          // Diese Spalten sind unbekannt
          fail(String.format("Spalte '%s' ist im Contract nicht bekannt!", columnName));
      }
    }
  }

  @Test
  public void checkAllIssueDataColumns_AgainstContract(){
    // Arrange

    // Act
    String[] columns = getMockContentResolver().query(
      TimeDataContract.IssueData.CONTENT_URI,
      null,
      TimeDataContract.IssueData.Columns._ID + "=?",
      new String[]{"1"},
      null)
      .getColumnNames();

    // Assert
    for (String columnName : columns) {
      switch (columnName){
        case TimeDataContract.IssueData.Columns._ID:
        case TimeDataContract.IssueData.Columns.NUMBER:
        case TimeDataContract.IssueData.Columns.TITLE:
        case TimeDataContract.IssueData.Columns.PRIORITY:
        case TimeDataContract.IssueData.Columns.STATE:
          // Diese Spalten werden erwartet
          break;

        default:
          // Diese Spalten sind unbekannt
          fail(String.format("Spalte '%s' ist im Contract nicht bekannt!", columnName));
      }
    }
  }

  private long createNewEntry(){
    final String startDate = "2017-05-31T22:47";
    final String endDate = "2017-06-01T07:29";
    final int pause = 40;
    final String comment = "Work at night";
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.START_TIME, startDate);
    values.put(TimeDataContract.TimeData.Columns.END_TIME, endDate);
    values.put(TimeDataContract.TimeData.Columns.PAUSE, pause);
    values.put(TimeDataContract.TimeData.Columns.COMMENT, comment);

    // Act
    Uri insertUri = getMockContentResolver().insert(
      TimeDataContract.TimeData.CONTENT_URI,
      values);

    return ContentUris.parseId(insertUri);
  }
}
