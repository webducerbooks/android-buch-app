package de.webducer.androidbuch.zeiterfassung;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsAnything.anything;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppNavigationTests {

  @Rule
  public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

  @Test
  public void appNavigationTests_FromTestRecorder() {
    ViewInteraction button = onView(
      allOf(withId(R.id.StartCommand),
        childAtPosition(
          childAtPosition(
            withId(android.R.id.content),
            0),
          4),
        isDisplayed()));
    button.check(matches(isDisplayed()));

    ViewInteraction appCompatButton = onView(
      allOf(withId(R.id.StartCommand), withText("Start"), isDisplayed()));
    appCompatButton.perform(click());

    ViewInteraction appCompatButton2 = onView(
      allOf(withId(R.id.EndCommand), withText("End"), isDisplayed()));
    appCompatButton2.perform(click());

    ViewInteraction actionMenuItemView = onView(
      allOf(withId(R.id.ListDataMenuItem), withContentDescription("Data List"), isDisplayed()));
    actionMenuItemView.perform(click());

    ViewInteraction linearLayout = onView(
      allOf(childAtPosition(
        withId(R.id.DataList),
        0),
        isDisplayed()));
    linearLayout.perform(click());

    ViewInteraction textView = onView(
      allOf(withId(R.id.CommentLabel), withText("Comment:"),
        childAtPosition(
          childAtPosition(
            withId(android.R.id.content),
            0),
          9),
        isDisplayed()));
    textView.check(matches(withText("Comment:")));

    ViewInteraction appCompatImageButton = onView(
      allOf(withContentDescription("Navigate up"),
        withParent(allOf(withId(R.id.action_bar),
          withParent(withId(R.id.action_bar_container)))),
        isDisplayed()));
    appCompatImageButton.perform(click());

    pressBack();

    ViewInteraction button2 = onView(
      allOf(withId(R.id.EndCommand),
        childAtPosition(
          childAtPosition(
            withId(android.R.id.content),
            0),
          5),
        isDisplayed()));
    button2.check(matches(isDisplayed()));

  }

  @Test
  public void appNavigationTests_Optimized(){
    // Testen auf das Vorhandensein des Start-Buttons
    ViewInteraction startButton = onView(withId(R.id.StartCommand));
    startButton.check(matches(isDisplayed()));

    // Klick auf den Startbutton
    startButton.perform(click());

    // Beenden-Button anklicken
    ViewInteraction endButton = onView(withId(R.id.EndCommand));
    endButton.perform(click());

    // Listen-Menü anklicken
    ViewInteraction listAction = onView(withId(R.id.ListDataMenuItem));
    listAction.perform(click());

    // Erstes Element in der Liste anklicken
    DataInteraction firstElement = onData(anything()).inAdapterView(withId(R.id.DataList))
      .atPosition(0);
    firstElement.perform(click());

    // Prüfen auf den Kommentar-Text
    ViewInteraction comment = onView(withId(R.id.CommentLabel));
    comment.check(matches(withText(R.string.LabelComment)));

    // Navigieren eine Ebene nach oben
    ViewInteraction navigate = onView(
      allOf(withContentDescription(R.string.abc_action_bar_up_description),
        withParent(allOf(withId(R.id.action_bar),
          withParent(withId(R.id.action_bar_container)))),
        isDisplayed()));
    navigate.perform(click());

    // Navigation zurück über Hardwarebutton
    pressBack();

    // Testen auf das Vorhandensein des Start-Buttons
    endButton = onView(withId(R.id.EndCommand));
    endButton.check(matches(isDisplayed()));
  }

  private static Matcher<View> childAtPosition(
    final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup && parentMatcher.matches(parent)
          && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
