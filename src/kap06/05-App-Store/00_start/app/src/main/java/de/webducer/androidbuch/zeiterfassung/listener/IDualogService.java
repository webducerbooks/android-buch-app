package de.webducer.androidbuch.zeiterfassung.listener;

import android.support.annotation.NonNull;

import java.util.Calendar;

public interface IDualogService {
  /**
   * Dialog für die Änderung des Datums öffnen
   *
   * @param originDate Datum, das geändert werden soll
   * @param fieldId    ID des Feldes für das Binding
   */
  void changeDate(@NonNull Calendar originDate, int fieldId);

  /**
   * Dialog für die Änderung der uhrzeit öffnen
   *
   * @param originTime Datum, das geändert werden soll
   * @param fieldId    ID des Feldes für das Binding
   */
  void changeTime(@NonNull Calendar originTime, int fieldId);

  /**
   * Callback, um die Änderung des Datums / Uhrzeit zu signalisieren
   *
   * @param newDateTime Neues Datum oder Uhrzeit
   * @param fieldId     ID des Feldes, das über Binding geändert werden soll
   */
  void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldId);
}
