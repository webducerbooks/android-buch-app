package de.webducer.androidbuch.zeiterfassung.utils;


import java.util.Calendar;

public class CalendarComparer {
  public static boolean equalsTillMinute(Calendar one, Calendar two){
    if (one == null && two == null){
      return true;
    }

    if(one == null && two != null){
      return false;
    }

    if(one != null && two == null){
      return false;
    }

    if (one.get(Calendar.YEAR) != two.get(Calendar.YEAR)){
      return false;
    }

    if (one.get(Calendar.MONTH) != two.get(Calendar.MONTH)){
      return false;
    }

    if (one.get(Calendar.DAY_OF_MONTH) != two.get(Calendar.DAY_OF_MONTH)){
      return false;
    }

    if (one.get(Calendar.HOUR_OF_DAY) != two.get(Calendar.HOUR_OF_DAY)){
      return false;
    }

    if (one.get(Calendar.MINUTE) != two.get(Calendar.MINUTE)){
      return false;
    }

    return true;
  }
}
