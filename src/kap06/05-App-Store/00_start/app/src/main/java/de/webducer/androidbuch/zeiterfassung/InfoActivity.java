package de.webducer.androidbuch.zeiterfassung;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.io.IOException;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.utils.IssueCursorAdapter;
import de.webducer.androidbuch.zeiterfassung.utils.IssueHandler;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class InfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

  private final static int _ISSUE_LOADER = 100;
  private ListView _list;
  private SimpleCursorAdapter _adapter;
  private Call _updateCall = null;

  // Spalten zum Laden
  private static final String[] _COLUMNS = new String[]{
      BaseColumns._ID,
      TimeDataContract.IssueData.Columns.NUMBER,
      TimeDataContract.IssueData.Columns.TITLE,
      TimeDataContract.IssueData.Columns.PRIORITY,
      TimeDataContract.IssueData.Columns.STATE
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Intialisierung der UI Elemente
    _list = (ListView) findViewById(R.id.IssueList);

    // Zuweisung des Adapters
    _adapter = new IssueCursorAdapter(this, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    _list.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    getSupportLoaderManager().restartLoader(_ISSUE_LOADER, null, this);

    // Aktualisieren der Daten aus dem Internet
    updateData();
  }

  @Override
  protected void onStop() {
    super.onStop();

    getSupportLoaderManager().destroyLoader(_ISSUE_LOADER);

    // Update der daten aus dem Internet abbrechen, falls es nocht nicht abgeschlossen wurde
    if(_updateCall != null){
      _updateCall.cancel();
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    CursorLoader loader = null;

    switch (id) {
      case _ISSUE_LOADER:
        loader = new CursorLoader(
            this,
            TimeDataContract.IssueData.CONTENT_URI,
            _COLUMNS, // Spalten
            null,
            null,
            TimeDataContract.IssueData.Columns.NUMBER + " DESC"); // Sortierung
    }

    return loader;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    switch (loader.getId()) {
      case _ISSUE_LOADER:
        _adapter.swapCursor(data);
        break;
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    switch (loader.getId()) {
      case _ISSUE_LOADER:
        _adapter.swapCursor(null);
        break;
    }
  }

  private void updateData() {
    // Client initialisieren
    OkHttpClient client = new OkHttpClient();

    // Request erstellen
    Request request = new Request.Builder()
        .url(IssueHandler.FILTERED_ISSUE_URI)
        .build();

    // Asynchronen Aufruf erstellen
    _updateCall = client.newCall(request);
    _updateCall.enqueue(new Callback() {
      @Override
      public void onFailure(Call call, IOException e) {
        // Behandlung im Fehlerfall
        e.printStackTrace();
      }

      @Override
      public void onResponse(Call call, Response response) throws IOException {
        // Behandlung im positiven Fall
        if (!response.isSuccessful()) {
          // Fehler bei der Abfrage der Daten
          return;
        }

        // Zurückgeben der Daten
        // Parsen mit GSon
        BitbucketIssue[] issues = IssueHandler.parseWithGSon(response.body().string());

        // Speichern in die Datenbank
        IssueHandler.saveToDb(getApplicationContext(), issues);
      }
    });
  }
}
