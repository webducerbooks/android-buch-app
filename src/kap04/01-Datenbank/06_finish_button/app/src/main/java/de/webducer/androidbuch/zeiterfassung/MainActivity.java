package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  private DateFormat _dateTimeFormatter = DateFormat.getDateTimeInstance(
    DateFormat.SHORT, DateFormat.SHORT);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Eingabefeld für Startzeit suchen
    _startDateTime = (EditText) findViewById(R.id.StartDateTime);
    _endDateTime = (EditText) findViewById(R.id.EndDateTime);
    _startCommand = (Button) findViewById(R.id.StartCommand);
    _endCommand = (Button) findViewById(R.id.EndCommand);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Button Klick registrieren
    _startCommand.setOnClickListener(new StartButtonClicked());
    _endCommand.setOnClickListener(new EndButtonClicked());
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Buttin Klick deregisitrieren
    _startDateTime.setOnClickListener(null);
    _endDateTime.setOnClickListener(null);
  }

  // Klasse für die Behandlung des Klicks auf den Start-Button
  class StartButtonClicked implements View.OnClickListener {

    @Override
    public void onClick(View v) {
      // Aktuelles Datum und Uhrzeit
      Calendar currentTime = Calendar.getInstance();
      // Formatierung in ISO 8601
      String dbCurrentTime =
        TimeDataContract.Converter.format(currentTime);
      // Zuordnen der Zeit der Startzeit Spalte
      ContentValues values = new ContentValues();
      values.put(
        TimeDataContract.TimeData.Columns.START_TIME,
        dbCurrentTime);
      // Einfügen des Datensatzen in die Datenbank
      getContentResolver()
        .insert(TimeDataContract.TimeData.CONTENT_URI, values);
      // Ausgabe des aktuellen Datums auf der Oberfläche
      _startDateTime.setText(
        _dateTimeFormatter.format(currentTime.getTime()));
    }
  }

  // Klasse für die Behandlung des Klicks auf den Beenden-Button
  class EndButtonClicked implements View.OnClickListener {

    @Override
    public void onClick(View v) {
      // Aktuelles Datum und Uhrzeit
      Calendar currentTime = Calendar.getInstance();
      // Formatierung in ISO 8601
      String dbCurrentTime = TimeDataContract.Converter.format(currentTime);
      // Zuordnen der Zeit dem Endzeit Spalte
      ContentValues values = new ContentValues();
      values.put(TimeDataContract.TimeData.Columns.END_TIME, dbCurrentTime);
      // Aktualisieren des offenen Datensatzes in der Datenbank
      getContentResolver().update(TimeDataContract.TimeData.NOT_FINISHED_CONTENT_URI,
        values, null, null);
      // Ausgabe des aktuellen Datums auf der Oberfläche
      _endDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));
    }
  }
}
