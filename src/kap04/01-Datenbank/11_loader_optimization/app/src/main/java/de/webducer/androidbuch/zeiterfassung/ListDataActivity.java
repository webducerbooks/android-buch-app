package de.webducer.androidbuch.zeiterfassung;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class ListDataActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
  // Klassenvariablen
  private ListView _list;
  private SimpleCursorAdapter _adapter; // Adapter für die Listendarstellung
  private final static int _DATA_LOADER_ID = 100; // ID des Loaders (Hintergrundladen)
  private DateFormat _dateTimeFormatter = DateFormat.getDateTimeInstance(
    DateFormat.SHORT, DateFormat.SHORT); // Formatter für Datum und Uhrzeit

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_data);

    // Suchen der Liste im Layout
    _list = (ListView) findViewById(R.id.DataList);

    // Initialisierung des Adapters
    _adapter = new SimpleCursorAdapter(
      this, // Context
      R.layout.row_time_data, // Layout für die Zeile
      null, // Daten für die Darstellung
      new String[]{
        TimeDataContract.TimeData.Columns.START_TIME,
        TimeDataContract.TimeData.Columns.END_TIME
      }, // Darzustellende Spalten
      new int[]{
        R.id.StartTimeValue,
        R.id.EndTimeValue
      }, // IDs der Views für die Darstellung
      android.support.v4.widget.SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
    ); // Änderungen an den Daten beobachten

    // Formatierung der Daten im Adapter beeinflussen
    _adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
      @Override
      public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        // Nur TextViews verarbeiten
        if((view instanceof TextView) == false){
          return false;
        }

        // Wert auf NULL in der Datenbank prüfen
        if (cursor.isNull(columnIndex)){
          ((TextView)view).setText(R.string.EmptyValuePlaceholder);
          return true;
        }

        // Wert als Datum parsen und formatieren
        String value;
        try {
          Calendar date = TimeDataContract.Converter.parse(cursor.getString(columnIndex));
          // Formatieren der Ausgabe
          value = _dateTimeFormatter.format(date.getTime());
        } catch (ParseException e) {
          // Wert kann nicht in Datum umgewandelt werden
          value = getString(R.string.DateParseErrorMessage);
        }

        ((TextView)view).setText(value);

        return true;
      }
    });

    // Adapter der Liste zuordnen
    _list.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Loader starten
    getSupportLoaderManager().restartLoader(
      _DATA_LOADER_ID, // ID des Loaders, der gestartet werden soll
      null, // keine Zusatzinformationen an Loader übergeben
      this); // Klasse, die das Loader-Interface implementiert
  }

  @Override
  protected void onStop() {
    super.onStop();

    // Loader freigeben
    getSupportLoaderManager().destroyLoader(_DATA_LOADER_ID);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
    CursorLoader loader = null;

    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loaderId) {
      case _DATA_LOADER_ID:
        loader = new CursorLoader(
          this, // Context
          TimeDataContract.TimeData.CONTENT_URI, // URI für den Content Provider
          new String[]{
            TimeDataContract.TimeData.Columns._ID,
            TimeDataContract.TimeData.Columns.START_TIME,
            TimeDataContract.TimeData.Columns.END_TIME
          }, // Zu ladende Spalten
          null, // Filter
          null, // Filter Argumente
          TimeDataContract.TimeData.Columns.START_TIME + " DESC" // Sortierung
        );
        break;
    }

    return loader;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(data); // Austauschen der Daten gegen neue im Adapter
        break;
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(null); // Daten freigeben
        break;
    }
  }
}
