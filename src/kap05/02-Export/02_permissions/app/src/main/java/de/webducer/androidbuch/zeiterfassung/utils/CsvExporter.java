package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class CsvExporter extends AsyncTask<Void, Integer, Void> {

  private final Context _context;

  public CsvExporter(Context context) {
    _context = context;
  }

  @Override
  protected Void doInBackground(Void... params) {
    Cursor data = null;

    try {
      // Abfrage aller Daten aus der Datenbak
      data = _context.getContentResolver()
          .query(TimeDataContract.TimeData.CONTENT_URI, // Uri zu Daten
              null, // Alle Spalten
              null, // Filter
              null, // Filter Argumente
              null); // Sortierung

      // Anzahl der Datensätze bestimmen
      int dataCount = data == null ? 0 : data.getCount();

      // Abbrechen, falls keine Daten vorhanden
      if (dataCount == 0) {
        return null;
      }

      // Extern erreichbaren Speicherort bestimmen
      File externalStorage = Environment.getExternalStorageDirectory();

      // Prüfen, ob dieser Speicherort beschreibbar ist
      if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
        return null;
      }

      // Export Unterverzeichnis
      File exportPath = new File(externalStorage, "export");

      // Export Datei
      File exportFile = new File(exportPath, "TimeDataLog.csv");

      // Anlegen, der eventuell nicht vorhandener Unterordner
      if (!exportFile.exists()) {
        exportPath.mkdirs();
      }

      // Writer für die CSV Datei
      BufferedWriter writer = null;
      try {
        // Writer initialisieren
        writer = new BufferedWriter(new FileWriter(exportFile));

        // Lesen der Spaltennamen aus der Ergenbismenge
        String[] columnList = data.getColumnNames();

        // Eine Zeile in der CSV-Datei
        StringBuilder line = new StringBuilder();

        // Zusammensetzen der Spaltenzeile
        for (String columnName : columnList) {
          // ";" vorsetzen, falls nicht die erste Spalte
          if (line.length() > 0) {
            line.append(';');
          }

          // Spaltennamen schreiben
          line.append(columnName);
        }

        // Schreiben der Zeile in die Datei
        writer.append(line);

        // Lesen aller Daten aus der Ergebnismenge
        while (data.moveToNext()) {
          // Neue Zeile zur Datei hinzufügen
          writer.newLine();

          // Leeren des Inhaltes in der Zeilenvariable
          line.delete(0, line.length());

          // Spaltenwerte auslesen
          for (int columnIndex = 0; columnIndex < columnList.length; columnIndex++) {
            // ";" vorsetzen, falls nicht die erste Spalte
            if (line.length() > 0) {
              line.append(';');
            }

            // Prüfen auf "NULL"-Wert
            if (data.isNull(columnIndex)) {
              line.append("<NULL>");
            } else {
              line.append(data.getString(columnIndex));
            }
          }

          // Schreiben der Zeile in die Datei
          writer.append(line);
        }
      } catch (IOException e) {
        // Fehler beim Schreiben in die Datei
        e.printStackTrace();
      } finally {
        try {
          if (writer != null) {
            // Daten aus dem Arbeitsspeicher (Cache) in die Datei speichern
            writer.flush();

            // Ressourcen für Writer freigeben
            writer.close();
          }
        } catch (IOException e) {
          // Beim Bereinigen des Writers ist etwas schief gelaufen
          e.printStackTrace();
        }
      }

      return null;
    } finally {
      // Schließen des Zeicher nach getanner Arbeit
      if (data != null) {
        data.close();
      }
    }
  }
}
