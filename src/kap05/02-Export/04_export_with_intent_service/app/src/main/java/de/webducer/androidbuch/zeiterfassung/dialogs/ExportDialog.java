package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;

import de.webducer.androidbuch.zeiterfassung.R;
import de.webducer.androidbuch.zeiterfassung.utils.ExportService;

public class ExportDialog extends AppCompatDialogFragment {
  private ProgressDialog _dialog;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {

    // Dialog initialisieren
    _dialog = new ProgressDialog(getContext());
    // Dialog Title
    _dialog.setTitle(R.string.ExportDialogTitle);
    // Dialog Text
    _dialog.setMessage(getString(R.string.ExportDialogMessage));
    // Schließen durch "daneben" Tippen, verhindern
    _dialog.setCanceledOnTouchOutside(false);
    // Abbrechen durch den Zurückbutton
    _dialog.setCancelable(true);
    // Typ des Dialoges festlegen (Allgemein oder Fortschritt)
    _dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    // Abbrechen Button hinzufügen
    _dialog.setButton(Dialog.BUTTON_NEGATIVE,
      getString(R.string.CancelButton),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          // Export Service mitteielen, dass die Aktion abgebrochen werden soll
          cancelExport();
        }
      });

    return _dialog;
  }

  private void cancelExport() {
    // Senden an den Export Service, dass der Export abgebrochen wurde
    Intent cancelIntent = new Intent(getContext(), ExportService.class);
    cancelIntent.setAction(ExportService.ACTION_CANCEL_EXPORT);
    getContext().startService(cancelIntent);
  }
}
