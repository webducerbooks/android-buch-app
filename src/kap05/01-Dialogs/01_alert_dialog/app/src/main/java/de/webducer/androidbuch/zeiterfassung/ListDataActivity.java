package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.utils.DateTimeViewBinder;

public class ListDataActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
  // Klassenvariablen
  private ListView _list;
  private SimpleCursorAdapter _adapter; // Adapter für die Listendarstellung
  private final static int _DATA_LOADER_ID = 100; // ID des Loaders (Hintergrundladen)
  private final static String[] _LOAD_COLUMNS = new String[]{
      TimeDataContract.TimeData.Columns._ID,
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME
  }; // Spalten zum Laden
  private final static String[] _VISIBLE_COLUMNS = new String[]{
      TimeDataContract.TimeData.Columns.START_TIME,
      TimeDataContract.TimeData.Columns.END_TIME
  }; // Sichtbare Spalten
  private final static int[] _ROW_VIEW_IDS = new int[]{
      R.id.StartTimeValue,
      R.id.EndTimeValue
  }; // IDs für Zeilen-Views
  private final static String _SORT_ORDER =
      TimeDataContract.TimeData.Columns.START_TIME + " DESC";


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_data);

    // Suchen der Liste im Layout
    _list = (ListView) findViewById(R.id.DataList);

    // Initialisierung des Adapters
    _adapter = new SimpleCursorAdapter(
        this, // Context
        R.layout.row_time_data, // Layout für die Zeile
        null, // Daten für die Darstellung
        _VISIBLE_COLUMNS, // Darzustellende Spalten
        _ROW_VIEW_IDS, // IDs der Views für die Darstellung
        android.support.v4.widget.SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
    ); // Änderungen an den Daten beobachten

    // Formatierung der Daten im Adapter beeinflussen
    _adapter.setViewBinder(new DateTimeViewBinder());

    // Adapter der Liste zuordnen
    _list.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Loader starten
    getSupportLoaderManager().restartLoader(
        _DATA_LOADER_ID, // ID des Loaders, der gestartet werden soll
        null, // keine Zusatzinformationen an Loader übergeben
        this); // Klasse, die das Loader-Interface implementiert
  }

  @Override
  protected void onStop() {
    super.onStop();

    // Loader freigeben
    getSupportLoaderManager().destroyLoader(_DATA_LOADER_ID);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Registrieren des Kontextmenüs für die Liste
    registerForContextMenu(_list);
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Deregistrieren des Kontextmenüs für die Liste
    unregisterForContextMenu(_list);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    switch (v.getId()) {
      case R.id.DataList:
        // Kontextmenü für die Liste aufbauen
        getMenuInflater().inflate(R.menu.ctx_menu_data_list, menu);
        break;
    }
    super.onCreateContextMenu(menu, v, menuInfo);
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.DeleteMenuItem:
        // Metainformationen zum Menüpunkt auslesen
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        // Auslesen der ID aus den Metainformationen
        final long id = info.id;

        // Dialog initialisieren
        AlertDialog dialog = new AlertDialog.Builder(this) // Context
            .setTitle(R.string.DialogTitleDeleteItem) // Titel des Dialoges
            .setMessage(R.string.DialogMessageDeleteItem) // Nachricht des Dialoges
            .setPositiveButton(R.string.DeleteButton, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                // Uri zum Löschen generieren
                Uri deleteUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, id);

                // Löschen über Content Provider
                getContentResolver().delete(deleteUri, null, null);
              }
            }) // Löschen Button (positive Antwort)
            .setNegativeButton(R.string.CancelButton, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                // Dialog einfach schließen
                dialog.dismiss();
              }
            }) // Abbrechen Button (negative Antwort)
            .create();

        // Dialog anzeigen
        dialog.show();
        return true;

      default:
        return super.onContextItemSelected(item);
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
    CursorLoader loader = null;

    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loaderId) {
      case _DATA_LOADER_ID:
        loader = new CursorLoader(
            this, // Context
            TimeDataContract.TimeData.CONTENT_URI, // URI für den Content Provider
            _LOAD_COLUMNS, // Zu ladende Spalten
            null, // Filter
            null, // Filter Argumente
            _SORT_ORDER // Sortierung
        );
        break;
    }

    return loader;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(data); // Austauschen der Daten gegen neue im Adapter
        break;
    }
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    // Unterscheidung zwischen unterschiedlichen Loadern
    switch (loader.getId()) {
      case _DATA_LOADER_ID:
        _adapter.swapCursor(null); // Daten freigeben
        break;
    }
  }

}
