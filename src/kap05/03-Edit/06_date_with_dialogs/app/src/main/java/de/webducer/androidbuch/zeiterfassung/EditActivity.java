package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateDialog;
import de.webducer.androidbuch.zeiterfassung.listener.IChangeDateTimeListener;

public class EditActivity extends AppCompatActivity implements IChangeDateTimeListener {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";

  // Konstanten für die Zustandssicherung
  private static final String _START_DATE_TIME_KEY = "StartTimeKey";
  private static final String _END_DATE_TIME_KEY = "EndTimeKey";
  private static final String _PAUSE_KEY = "PauseKey";
  private static final String _COMMENT_KEY = "CommentKey";

  // Klassenvariablen
  private static final long _NO_DATA = -1L;
  private long _timeTadaId = _NO_DATA;

  // Datumsfelder
  private Calendar _startDateTime = Calendar.getInstance();
  private Calendar _endDateTime = Calendar.getInstance();

  // Sicherungsfeld
  private boolean _isRestored = false;

  // UI Elemente
  private EditText _startDate;
  private EditText _endDate;
  private EditText _pause;
  private EditText _comment;
  private EditText _startTime;
  private EditText _endTime;

  // Formatter
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit);

    // Initialisierung der Formatter
    _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);

    // Initialisieren der UI Elemente
    _startDate = (EditText) findViewById(R.id.StartDate);
    _startTime = (EditText) findViewById(R.id.StartTime);
    _endDate = (EditText) findViewById(R.id.EndDate);
    _endTime = (EditText) findViewById(R.id.EndTime);
    _pause = (EditText) findViewById(R.id.Pause);
    _comment = (EditText) findViewById(R.id.Comment);

    // Auslesen der übergebenen Metainformationen, falls welche da sind
    _timeTadaId = getIntent().getLongExtra(
        ID_KEY, // Key für die metainformation
        _NO_DATA); // Standardwert, falls nichts übergeben wurde

    // Wiederherstellen-Flag der Daten
    if (savedInstanceState != null && savedInstanceState.containsKey(_START_DATE_TIME_KEY)) {
      // Flag für Wiederherstellung
      _isRestored = true;
    }
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Daten laden, wenn Daten nicht wiederhergestellt wurden
    if (!_isRestored) {
      loadData();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Listener registrieren
    _startDate.setOnLongClickListener(new OnDateClicked(IChangeDateTimeListener.START_DATE));
    _endDate.setOnLongClickListener(new OnDateClicked(IChangeDateTimeListener.END_DATE));
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Listener deregistrieren
    _startDate.setOnLongClickListener(null);
    _endDate.setOnLongClickListener(null);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    // Speichern der Daten, wenn der Benutzer zurück geht
    saveData();
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    // Wiederherstellen der Daten
    if (savedInstanceState != null && savedInstanceState.containsKey(_START_DATE_TIME_KEY)) {
      // Datumsfelder aus der Anzahl der Millisekunden
      _startDateTime.setTimeInMillis(savedInstanceState.getLong(_START_DATE_TIME_KEY));
      _endDateTime.setTimeInMillis(savedInstanceState.getLong(_END_DATE_TIME_KEY));

      // Pause, falls gesichert (mit Standardwert)
      int pause = savedInstanceState.getInt(_PAUSE_KEY, 0);
      _pause.setText(String.valueOf(pause));

      // Kommentar
      _comment.setText(savedInstanceState.getString(_COMMENT_KEY));

      displayDateTime();
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    // Datumsfelder als Anzahl der Millisekunden merken
    outState.putLong(_START_DATE_TIME_KEY, _startDateTime.getTimeInMillis());
    outState.putLong(_END_DATE_TIME_KEY, _endDateTime.getTimeInMillis());

    // Pause nach der Konvertierung in Integer merken
    String pauseValue = _pause.getText().toString();
    if (pauseValue != null && !pauseValue.isEmpty()) {
      int pause = Integer.parseInt(pauseValue);
      outState.putInt(_PAUSE_KEY, pause);
    }

    // Kommentar sichern
    outState.putString(_COMMENT_KEY, _comment.getText().toString());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Zurück-Button in Actionbar
      case android.R.id.home:
        saveData();

        // Kein break oder return, da dieses Menü von Android noch verarbeitet werden soll

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void loadData() {
    Uri dataUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeTadaId);
    Cursor data = getContentResolver().query(dataUri, // URI
        null, // Spalten
        null, // Filter
        null, // Filter Argumente
        null); // Sortierung

    // Prüfen, ob Daten zu der ID vorhanden sind
    if (data.moveToFirst()) {
      // Auslesen der Startzeit
      String startDateString = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME));
      try {
        _startDateTime = TimeDataContract.Converter.parse(startDateString);
      } catch (ParseException e) {
        // Startzeit konnte nicht interpretiert werden
        e.printStackTrace();
      }

      // Auslesen der Endzeit
      int columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
      if (!data.isNull(columnIndex)) {
        String endDateString = data.getString(columnIndex);
        try {
          _endDateTime = TimeDataContract.Converter.parse(endDateString);
        } catch (ParseException e) {
          // Endzeit konnte nicht interpretiert werden
          e.printStackTrace();
        }
      }

      // Auslesen und Ausgabe der Pause
      int pause = data.getInt(data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE));
      _pause.setText(String.valueOf(pause));

      // Auslesen und Ausgabe des Kommentars
      columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.COMMENT);
      if (!data.isNull(columnIndex)) {
        _comment.setText(data.getString(columnIndex));
      }
    }

    // Datumsfelder aktualisieren
    displayDateTime();

    // Ressourcen freigeben
    data.close();
  }

  private void displayDateTime() {
    // Ausgabe der Startzeit
    _startDate.setText(_dateFormatter.format(_startDateTime.getTime()));
    _startTime.setText(_timeFormatter.format(_startDateTime.getTime()));

    // Ausgabe Endzeit
    _endDate.setText(_dateFormatter.format(_endDateTime.getTime()));
    _endTime.setText(_timeFormatter.format(_endDateTime.getTime()));
  }

  private void saveData() {
    // Pause auslesen
    String pauseValue = _pause.getText().toString();
    int pause = 0;
    if (pauseValue != null && !pauseValue.isEmpty()) {
      pause = Integer.parseInt(pauseValue);
    }

    // Werte den Spalten zuordnen
    ContentValues values = new ContentValues();
    values.put(TimeDataContract.TimeData.Columns.START_TIME,
        TimeDataContract.Converter.format(_startDateTime));
    values.put(TimeDataContract.TimeData.Columns.END_TIME,
        TimeDataContract.Converter.format(_endDateTime));
    values.put(TimeDataContract.TimeData.Columns.PAUSE, pause);
    values.put(TimeDataContract.TimeData.Columns.COMMENT,
        _comment.getText().toString());

    // Speichern der Werte in der Datenbank
    Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeTadaId);
    getContentResolver().update(updateUri, values, null, null);
  }

  @Override
  public Calendar getDate(String dialogType) {
    switch (dialogType) {
      case IChangeDateTimeListener.START_DATE:
      case IChangeDateTimeListener.START_TIME:
        return _startDateTime;

      case IChangeDateTimeListener.END_DATE:
      case IChangeDateTimeListener.END_TIME:
        return _endDateTime;

      default:
        return null;
    }
  }

  @Override
  public TextView getOutputView(String dialogType) {
    switch (dialogType) {
      case IChangeDateTimeListener.START_DATE:
        return _startDate;

      case IChangeDateTimeListener.START_TIME:
        return _startTime;

      case IChangeDateTimeListener.END_DATE:
        return _endDate;

      case IChangeDateTimeListener.END_TIME:
        return _endTime;

      default:
        return null;
    }
  }

  public class OnDateClicked implements View.OnLongClickListener {
    private final String _dialogType;

    public OnDateClicked(String dialogType) {
      _dialogType = dialogType;
    }

    @Override
    public boolean onLongClick(View v) {
      ChangeDateDialog dialog = new ChangeDateDialog();
      dialog.show(getSupportFragmentManager(), _dialogType);
      return true;
    }
  }
}
