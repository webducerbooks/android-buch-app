package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class EditActivity extends AppCompatActivity {
  // Konstanten
  /**
   * Key für die Übergabe der ID über die Extras des intents
   */
  public static final String ID_KEY = "TimeDataId";

  // Klassenvariablen
  private static final long _NO_DATA = -1L;
  private long _timeTadaId = _NO_DATA;

  // UI Elemente
  private EditText _startDate;
  private EditText _endDate;
  private EditText _pause;
  private EditText _comment;
  private EditText _startTime;
  private EditText _endTime;

  // Formatter
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit);

    // Initialisierung der Formatter
    _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);

    // Initialisieren der UI Elemente
    _startDate = (EditText) findViewById(R.id.StartDate);
    _startTime = (EditText) findViewById(R.id.StartTime);
    _endDate = (EditText) findViewById(R.id.EndDate);
    _endTime = (EditText) findViewById(R.id.EndTime);
    _pause = (EditText) findViewById(R.id.Pause);
    _comment = (EditText) findViewById(R.id.Comment);

    // Auslesen der übergebenen Metainformationen, falls welche da sind
    _timeTadaId = getIntent().getLongExtra(
      ID_KEY, // Key für die metainformation
      _NO_DATA); // Standardwert, falls nichts übergeben wurde
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Daten laden, wenn ein ID übergeben wurde
    if(_timeTadaId != _NO_DATA){
      loadData();
    }
  }

  private void loadData() {
    Uri dataUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeTadaId);
    Cursor data = getContentResolver().query(dataUri, // URI
        null, // Spalten
        null, // Filter
        null, // Filter Argumente
        null); // Sortierung

    // Prüfen, ob Daten zu der ID vorhanden sind
    if (data.moveToFirst()) {
      // Auslesen der Startzeit
      String startDateString = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME));
      Calendar startDateTime = Calendar.getInstance();
      try {
        startDateTime = TimeDataContract.Converter.parse(startDateString);
      } catch (ParseException e) {
        // Startzeit konnte nicht interpretiert werden
        e.printStackTrace();
      }

      // Ausgabe der Startzeit
      _startDate.setText(_dateFormatter.format(startDateTime.getTime()));
      _startTime.setText(_timeFormatter.format(startDateTime.getTime()));

      // Auslesen der Endzeit
      int columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
      Calendar endDateTime = Calendar.getInstance();
      if (!data.isNull(columnIndex)) {
        String endDateString = data.getString(columnIndex);
        try {
          endDateTime = TimeDataContract.Converter.parse(endDateString);
        } catch (ParseException e) {
          // Endzeit konnte nicht interpretiert werden
          e.printStackTrace();
        }
      }

      // Ausgabe Endzeit
      _endDate.setText(_dateFormatter.format(endDateTime.getTime()));
      _endTime.setText(_timeFormatter.format(endDateTime.getTime()));

      // Auslesen und Ausgabe der Pause
      int pause = data.getInt(data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE));
      _pause.setText(String.valueOf(pause));

      // Auslesen und Ausgabe des Kommentars
    }

    // Ressourcen freigeben
    data.close();
  }
}
